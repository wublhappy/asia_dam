import Main from '@/components/main';

export const dataDict = [
    {
      path: '/data-dict',
      name: 'DataDict',
      component: Main,
      meta: {
        title: '数据字典',
        alwaysShow: true,
        icon: 'icon iconfont iconyoujianguanli'
      },
      alwaysShow: true,
      children: [
        //  专业分类
        {
          path: '/data-dict/specialty-type-list',
          name: 'SpecialtyTypeList',
          meta: {
            title: '专业分类列表',
            privilege: [
              { title: '查询', name: 'specialty-type-list-query' },
              { title: '新增', name: 'specialty-type-list-add' },
              { title: '编辑', name: 'specialty-type-list-update' },
              { title: '删除', name: 'specialty-type-list-delete' },
              { title: '批量删除', name: 'specialty-type-list-batch-delete' }
            ]
          },
          component: () => import('@/views/business/data-dict/specialty-type-list.vue')
        },
        // 地区
        {
          path: '/data-dict/district-list',
          name: 'DistrictList',
          meta: {
            title: '地区列表',
            privilege: [
              { title: '查询', name: 'district-list-query' },
              { title: '新增', name: 'district-list-add' },
              { title: '编辑', name: 'district-list-update' },
              { title: '删除', name: 'district-list-delete' },
              { title: '批量删除', name: 'district-list-batch-delete' }
            ]
          },
          component: () => import('@/views/business/data-dict/district-list.vue')
        },
        // 导师类型
        {
          path: '/data-dict/teacher-type-list',
          name: 'TeacherTypeList',
          meta: {
            title: '导师类型列表',
            privilege: [
              { title: '查询', name: 'teacher-type-list-query' },
              { title: '新增', name: 'teacher-type-list-add' },
              { title: '编辑', name: 'teacher-type-list-update' },
              { title: '删除', name: 'teacher-type-list-delete' },
              { title: '批量删除', name: 'teacher-type-list-batch-delete' }
            ]
          },
          component: () => import('@/views/business/data-dict/teacher-type-list.vue')
        },
        // 资讯类别
        {
          path: '/data-dict/news-type-list',
          name: 'NewsTypeList',
          meta: {
            title: '资讯类型列表',
            privilege: [
              { title: '查询', name: 'news-type-list-query' },
              { title: '新增', name: 'news-type-list-add' },
              { title: '编辑', name: 'news-type-list-update' },
              { title: '删除', name: 'news-type-list-delete' },
              { title: '批量删除', name: 'news-type-list-batch-delete' }
            ]
          },
          component: () => import('@/views/business/data-dict/news-type-list.vue')
        }
      ]
    }
  ];