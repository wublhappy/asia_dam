import Main from '@/components/main';
// 基础设置
export const teacher = [
  {
    path: '/teacher',
    name: 'Teacher',
    component: Main,
    meta: {
      title: '教师管理',
      alwaysShow: true,
      icon: 'icon iconfont iconyoujianguanli'
    },
    alwaysShow: true,
    children: [
      //  教师列表
      {
        path: '/teacher/teacher-list',
        name: 'TeacherList',
        meta: {
          title: '教师列表',
          privilege: [
            { title: '查询', name: 'teacher-list-query' },
            { title: '新增', name: 'teacher-list-add' },
            { title: '编辑', name: 'teacher-list-update' },
            { title: '删除', name: 'teacher-list-delete' }
          ]
        },
        component: () => import('@/views/business/teacher/teacher-list.vue')
      }
    ]
  }
];
