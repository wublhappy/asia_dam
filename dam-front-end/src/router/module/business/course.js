import Main from '@/components/main';
// 基础设置
export const course = [
  {
    path: '/course',
    name: 'Course',
    component: Main,
    meta: {
      title: '课程管理',
      alwaysShow: true,
      icon: 'icon iconfont iconyoujianguanli'
    },
    alwaysShow: true,
    children: [
      //  课程列表
      {
        path: '/course/course-list',
        name: 'CourseList',
        meta: {
          title: '课程列表',
          privilege: [
            { title: '查询', name: 'course-query' },
            { title: '新增', name: 'course-add' },
            { title: '编辑', name: 'course-update' },
            { title: '删除', name: 'course-delete' }
          ]
        },
        component: () => import('@/views/business/course/course-list.vue')
      }
    ]
  }
];
