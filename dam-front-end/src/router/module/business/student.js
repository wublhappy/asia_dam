import Main from '@/components/main';
// 基础设置
export const student = [
  {
    path: '/student',
    name: 'Student',
    component: Main,
    meta: {
      title: '学生管理',
      alwaysShow: true,
      icon: 'icon iconfont iconyoujianguanli'
    },
    alwaysShow: true,
    children: [
      //  学生列表
      {
        path: '/student/student-list',
        name: 'StudentList',
        meta: {
          title: '学生列表',
          privilege: [
            { title: '查询', name: 'student-list-query' },
            { title: '新增', name: 'student-list-add' },
            { title: '编辑', name: 'student-list-update' },
            { title: '删除', name: 'student-list-delete' }
          ]
        },
        component: () => import('@/views/business/student/student-list.vue')
      }
    ]
  }
];