import { postAxios, getAxios, getDownloadAxios, postDownloadAxios } from '@/lib/http';
export const damHomeApi = {
  // 文件树
  getFiles: (data) => {
    return postAxios('/category/query', data);
  },
  // 搜索
  assetSearch: (data) => {
    return postAxios('/asset/search', data);
  },
  // 系统文件下载接口
  downLoadFile: id => {
    return getDownloadAxios('/asset/file/downLoad?id=' + id);
  },
  // 系统多文件下载接口
  downLoadFileMuti: (data) => {
    return postDownloadAxios('/asset/file/multidownload', data);
  },
  // 获取文件详情
  getFileDetail: id => {
    return getAxios('/asset/query/' + id);
  },
  // 顶级目录
  getQueryRoot: (data) => {
    return postAxios('/category/queryRoot', data);
  },
  // 子集目录
  getQueryChildren: (data) => {
    return postAxios(`/category/${data.id}/queryChildren`, {});
  },
  // 面包屑
  getQueryAllParent: (data) => {
    return postAxios(`/category/${data.id}/queryAllParent`, {});
  },
  // 下载文件数量
  getSelectByKey: id => {
    return getAxios('systemConfig/selectByKey?configKey=' + id);
  },
};
