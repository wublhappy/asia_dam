import { postAxios, getAxios, postDownloadAxios } from '@/lib/http';

export const courseApi = {
    // 分页查询课程 @author 卓大
    queryCourse: (data) => {
        return postAxios('/course/page/query', data);
    },
    // 批量删除课程 @author 卓大
    batchDeleteCourse: (idList) => {
        return postAxios('/course/deleteByIds', idList);
    },
    // 修改课程  @author 卓大
    updateCourse: (data) => {
        return postAxios('/course/update',data);
    },
    // 添加课程
    addCourse: (data) => {
        return postAxios('/course/add', data);
    },
};