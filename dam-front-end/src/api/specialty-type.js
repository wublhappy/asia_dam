import { postAxios, getAxios, postDownloadAxios } from '@/lib/http';

export const specialtyTypeApi = {
    // 分页查询课程 @author 卓大
    queryAll: () => {
        return postAxios('/specialtytype/queryall');
    },
    addSpecialtyType: (data) => {
        return postAxios('/specialtytype/add', data);
    },
    updateSpecialtyType: (data) => {
        return postAxios('/specialtytype/update', data);
    },
    batchDeleteSpecialtyType: (idList) => {
        return postAxios('/specialtytype/deleteByIds', idList);
    }
};