import { postAxios, getAxios, postDownloadAxios } from '@/lib/http';

export const courseTypeApi = {
    // 分页查询课程 @author 卓大
    queryAll: () => {
        return postAxios('/coursetype/queryall');
    }
};