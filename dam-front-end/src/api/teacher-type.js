import { postAxios, getAxios, postDownloadAxios } from '@/lib/http';

export const teacherTypeApi = {
    // 分页查询导师类型 @author 卓大
    queryAll: () => {
        return postAxios('/teachertype/queryall');
    },
    addTeacherType: (data) => {
        return postAxios('/teachertype/add', data);
    },
    updateTeacherType: (data) => {
        return postAxios('/teachertype/update', data);
    },
    batchDeleteTeacherType: (idList) => {
        return postAxios('/teachertype/deleteByIds', idList);
    }
};