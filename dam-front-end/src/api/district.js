import { postAxios, getAxios, postDownloadAxios } from '@/lib/http';

export const districtApi = {
    // 分页查询课程 @author 卓大
    queryAll: () => {
        return postAxios('/district/queryall');
    },
    // 添加地区
    addDistrict: (data) => {
        return postAxios('/district/add', data);
    },
    // 更新地区
    updateDistrict: (data) => {
        return postAxios('/district/update', data);
    },
    // 批量删除地区
    batchDeleteDistrict: (idList) => {
        return postAxios('/district/deleteByIds', idList);
    }
};