//循环出父节点 链式数据
export function toTreeData(data){
    //重组父类型
    let oldData = JSON.parse(JSON.stringify(data))
    // oldData.forEach(item => {
    //     item.label = item.title;
    //     if(item.children){
    //         item.children.forEach(it=>{
    //             it.label = it.title;
    //             if(it.children){
    //                 it.children.forEach(i=>{
    //                     i.label = i.title;
    //                 })
    //             }
    //         })
    //     }
    // });
    findChild(oldData);
    function findChild(data){
        if(data.length){
            data.forEach(item => {
                item.label = item.title;
                if(item.children){
                    findChild(item);
                }
                if(item.children ===null){
                    delete item.children
                }
            })
        }
    }
    return oldData;
}
//循环出父节点 链式数据
export function toTreeSelectData(data){
    //重组父类型
    let oldData = JSON.parse(JSON.stringify(data))
    oldData.forEach(item => {
        item.label = item.title;
        if(item.children){
            find(item);
        }
    });
    function find(item){
        item.label = item.title;
        if(item.children){
            item.children.forEach(i=>{
                find(i);
            })
        }
    }
    return oldData;
}

