import Main from '@/components/main';
// 基础设置
export const emailSetting = [
  {
    path: '/email',
    name: 'Email',
    component: Main,
    meta: {
      title: 'Email Mgmt.',
      icon: 'icon iconfont iconyoujianguanli'
    },
    children: [
      //  邮件接收者
      {
        path: '/email/email-list',
        name: 'EmailList',
        meta: {
          title: '邮件接收者',
          privilege: [
            { title: '查询', name: 'email-receiver-query' },
            { title: '新增', name: 'email-receiver-add' },
            { title: '编辑', name: 'email-receiver-update' },
            { title: '删除', name: 'email-receiver-delete' }
          ]
        },
        component: () => import('@/views/system/email/email-list.vue')
      },
      //  邮件模板
      {
        path: '/email/send-mail',
        name: 'SendMail',
        meta: {
          title: '邮件模板',
          privilege: [
            { title: '查询', name: 'email-template-query' },
            { title: '保存', name: 'email-template-save' }
          ]
        },
        component: () => import('@/views/system/email/send-mail.vue')
      },
      //  发送时间
      {
        path: '/email/email-time',
        name: 'EmailTime',
        meta: {
          title: '邮件提醒',
          privilege: [
            { title: '保存', name: 'email-alert-config-save' },
            { title: '查询', name: 'email-alert-config-query' }
          ]
        },
        component: () => import('@/views/system/email/email-time.vue')
      }
    ]
  }
];
