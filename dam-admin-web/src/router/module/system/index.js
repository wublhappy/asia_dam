
import Main from '@/components/main';


import { employee } from './employee';
import { file } from './file';
import { userLog } from './user-log';
import { systemSetting } from './system-setting';
import { baseData } from './base-data';
import { emailSetting } from './email';
import { digitalAssets } from './digital-assets';

// 业务
export const system = [
  {
    path: '/system',
    name: 'System',
    component: Main,
    meta: {
      title: '系统设置',
      topMenu: true,
      icon: 'icon iconfont iconxitongshezhi'
    },
    children: [
      ...digitalAssets,
      ...baseData,
      ...emailSetting,
      ...employee,
      // ...file,
      ...userLog,
      ...systemSetting
    ]
  }
];
