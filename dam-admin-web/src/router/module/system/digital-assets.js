import Main from '@/components/main';

export const digitalAssets = [{
  path: '/digital-assets',
  name: 'DigitalAssets',
  component: Main,
  meta: {
    title: 'Digital Asset Mgmt.',
    icon: 'icon ivu-icon ivu-icon-ios-folder-open-outline'
  },
  children: [{
    path: '/digital-assets/assets-seting',
    name: 'AssetsSeting',
    meta: {
      title: 'Asset Mgmt.',
      privilege: [
        { title: '查询', name: 'assets-list-query' },
        { title: '批量选择类型', name: 'assets-category-batch-select' },
        { title: '批量打标签', name: 'assets-tags-batch-update' },
        { title: '批量下载', name: 'assets-batch-download' },
        { title: '批量审核', name: 'assets-batch-audit' },
        { title: '批量删除', name: 'assets-batch-delete' },
        { title: '查看', name: 'assets-view' },
        { title: '编辑', name: 'assets-update' },
        { title: '类型查询', name: 'category-type-query' },
        { title: '数据字典查询', name: 'data-dict-query' },
        { title: '标签列表查询', name: 'label-list-query' }
      ]
    },
    component: () => import('@/views/system/digital-assets/assets-seting/assets-seting.vue')
  }, {
    path: '/digital-assets/file-setimg',
    name: 'FileSeting',
    meta: {
      title: 'File Mgmt.',
      privilege: [
        { title: '查询', name: 'filemanage-file-list-query' },
        { title: '新建文件夹', name: 'filemanage-create-folder' },
        { title: '上传', name: 'filemanage-file-upload' },
        { title: '下载', name: 'filemanage-file-download' },
        { title: '移动', name: 'filemanage-file-move' },
        { title: '重命名', name: 'filemanage-file-rename' },
        { title: '删除', name: 'filemanage-file-delete' }
      ]
    },
    component: () => import('@/views/system/digital-assets/file-seting/file-seting.vue')
  }, {
    path: '/digital-assets/file-relation',
    name: 'FileRelation',
    meta: {
      title: 'Asset Mapping Mgmt.',
      privilege: [
        { title: '查询', name: 'filemapping-log-list-query' },
        { title: '下载', name: 'filemapping-log-download' }
      ]
    },
    component: () => import('@/views/system/digital-assets/file-relation/file-relation.vue')
  },
  {
    path: '/digital-assets/file-template',
    name: 'FileTemplate',
    meta: {
      title: 'Mapping File Mgmt.',
      privilege: [
        { title: '查询', name: 'filemapping-list-query' },
        { title: '上传模板文件', name: 'filemapping-template-upload' },
        { title: '下载', name: 'filemapping-template-download' },
        { title: '删除', name: 'filemapping-template-delete' }
      ]
    },
    component: () => import('@/views/system/digital-assets/file-template/file-template.vue')
  }
  ]
}];
