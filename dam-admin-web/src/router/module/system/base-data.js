import Main from '@/components/main';
export const baseData = [
  {
    path: '/base-data',
    name: 'BaseData',
    component: Main,
    meta: {
      title: 'Asset Info Mgmt.',
      icon: 'icon ivu-icon ivu-icon-ios-apps'
    },
    children: [
      {
        path: '/base-data/type',
        name: 'BaseDataType',
        meta: {
          title: 'Category Mgmt.',
          privilege: [
            { title: '查询', name: 'base-type-query' },
            { title: '新增', name: 'base-type-add' },
            { title: '编辑', name: 'base-type-update' },
            { title: '删除', name: 'base-type-delete' }
          ]
        },
        component: () => import('@/views/system/base-data/type/type-list.vue')
      },
      {
        path: '/base-data/label',
        name: 'BaseDataLabel',
        meta: {
          title: 'Label Mgmt.',
          privilege: [
            { title: '新增', name: 'base-label-add' },
            { title: '编辑', name: 'base-label-update' },
            { title: '查询', name: 'base-label-query' },
            { title: '删除', name: 'base-label-delete' }
          ]
        },
        component: () => import('@/views/system/base-data/label/label-list.vue')
      }
    ]
  }
];
