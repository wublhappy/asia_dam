import Main from '@/components/main';
// 基础设置
export const news = [
  {
    path: '/news',
    name: 'News',
    component: Main,
    meta: {
      title: '资讯管理',
      alwaysShow: true,
      icon: 'icon iconfont iconyoujianguanli'
    },
    alwaysShow: true,
    children: [
      //  资讯列表
      {
        path: '/news/news-list',
        name: 'NewsList',
        meta: {
          title: '资讯列表',
          privilege: [
            { title: '查询', name: 'news-list-query' },
            { title: '新增', name: 'news-list-add' },
            { title: '编辑', name: 'news-list-update' },
            { title: '删除', name: 'news-list-delete' }
          ]
        },
        component: () => import('@/views/business/news/news-list.vue')
      }
    ]
  }
];