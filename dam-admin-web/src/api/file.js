import { postAxios, getAxios, getDownloadAxios,postDownloadAxios} from '@/lib/http';
import config from '@/config';
const baseUrl = config.baseUrl.apiUrl;
export const fileApi = {
  // 系统文件查询
  queryFileList: data => {
    return postAxios('/api/file/query', data);
  },
  //
  queryFileManageList: data => {
    return postAxios('/fileManage/query', data);
  },
  // 系统文件下载通过接口
  downLoadFile: id => {
    return getDownloadAxios('/api/file/downLoad?id=' + id);
  },
  // 文件上传
  fileUpload: (type, data) => {
    // return postAxios('/api/file/localUpload/' + type, data);
    return this.fileUploadUrl;
  },
  // 文件保存
  addFile: data => {
    return postAxios('/api/file/save', data);
  },

  //添加文件夹
  addFolder: data => {
    return postAxios('fileManage/createFolder', data);
  },
  //添加文件夹
  reNameFolder: data => {
    return postAxios('/fileManage/rename', data);
  },
  //删除文件
  deleteFile: data => {
    return postAxios('/fileManage/deleteMulti', data);
  },
  //移动文件
  moveFile: data => {
    return postAxios('/fileManage/move', data);
  },
  //文件下载
  downloadFile:(id) => {
    return getAxios('/api/file/downLoad?id='+id);
  },
  // 模板文件查询
  queryM: data => {
    return postAxios('/api/mappingfile/query', data);
  },
  //删除文件
  deleteM: data => {
    return postAxios('/api/mappingfile/delete', data);
  },
  //下载文件
  downFile: data => {
    return postDownloadAxios('/fileManage/file/download', data);
  },
  //下载文件模板
  downTemp: data => {
    return postDownloadAxios('/api/mappingfile/download', data);
  },
  // 上传路径：本地
  //fileUploadLocalUrl: baseUrl + 'api/file/localUpload/',
  fileUploadLocalUrl: baseUrl + 'fileManage/localUpload/',
  localMultiUpload: baseUrl + '/fileManage/localMultiUpload/',
  // 上传路径：阿里OSS
  fileUploadAliUrl: baseUrl + 'api/file/aliYunUpload/',
  // 上传路径：七牛
  fileUploadQiNiuUrl: baseUrl + 'api/file/qiNiuUpload/',
  //模板文件上传
  fileMappingUploadLocalUrl: baseUrl + 'api/mappingfile/upload',
};
