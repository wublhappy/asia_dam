import { postAxios, getAxios, postDownloadAxios } from '@/lib/http';

export const newsApi = {
    // 分页查询资讯 @author 卓大
    queryNews: (data) => {
        return postAxios('/news/page/query', data);
    },
    // 添加资讯
    addNews: (data) => {
        return postAxios('/news/add', data);
    },
    // 更新资讯
    updateNews: (data) => {
        return postAxios('/news/update', data);
    },
    // 批量删除资讯
    batchDeleteNews: (idList) => {
        return postAxios('/news/deleteByIds', idList);
    }
};