import { postAxios, getAxios, postDownloadAxios } from '@/lib/http';

export const newsTypeApi = {
    // 分页查询课程 @author 卓大
    queryAll: () => {
        return postAxios('/newstype/queryall');
    },
    // 添加资讯类别
    addNewsType: (data) => {
        return postAxios('/newstype/add', data);
    },
    // 更新资讯类别
    updateNewsType: (data) => {
        return postAxios('/newstype/update', data);
    },
    // 批量删除资讯类别
    batchDeleteNewsType: (idList) => {
        return postAxios('/newstype/deleteByIds', idList);
    }
};