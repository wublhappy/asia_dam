import { postAxios, getAxios, postDownloadAxios } from '@/lib/http';

export const studentApi = {
    // 分页查询学生 @author 卓大
    queryStudent: (data) => {
        return postAxios('/student/page/query', data);
    },
    // 批量删除学生 @author 卓大
    batchDeleteStuent: (idList) => {
        return postAxios('/student/deleteByIds', idList);
    },
    // 修改学生  @author 卓大
    updateStudent: (data) => {
        return postAxios('/student/update',data);
    },
    // 添加学生
    addStudent: (data) => {
        return postAxios('/student/add', data);
    },
    // 删除学生作品
    deleteProduct: (data) => {
        return postAxios('/student/deleteProduct', data);
    }
};