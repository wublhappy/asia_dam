import { postAxios, getAxios ,postDownloadAxios} from '@/lib/http';
export const assetsApi = {
  // 资产列表
  query: (data) => {
    return postAxios('/assets/query', data);
  },
  // 审核
  audit: (data) => {
    return postAxios('/assets/audit', data);
  },
  // 删除
  delete: (data) => {
    return postAxios('/assets/delete' ,data);
  },
  //查询
  queryOne(id){
    return getAxios('/asset/query/' + id);
  },
  update(data){
    return postAxios('/asset/update', data);
  },
  queryTag(id){
    return postAxios('/asset/'+id+'/tags');
  },
  //数据字典
  getdicData(data){
      return postAxios('/systemConfig/getList', data);
  },
  getOneGroup(data){
    return getAxios('/systemConfig/getListByGroup', data);
  },
  //批量更新类型
  mulUpdateType(data){
    return postAxios('/assets/category/update', data);
  },
  //批量更新标签
  mulUpdateLabel(data){
    return postAxios('/assets/tag/update', data);
  },
  //文件关联
  fileRelation(data){
    return postAxios('/assetMappingLog/query', data);
  },
  //批量下载
  multiDownload(data){
    return postDownloadAxios('/asset/download', data);
  },
  // ip列表
  getTagQuery(data){
    return postAxios('/tag/queryall', data);
  },
  // 顶级目录
  getQueryRoot: (data) => {
    return postAxios('/category/queryRoot', data);
  },
  // 子集目录
  getQueryChildren: (data) => {
    return postAxios(`/category/${data.id}/queryChildren`, {});
  },
};
