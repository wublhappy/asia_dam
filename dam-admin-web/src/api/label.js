import {
  postAxios,
  getAxios
} from '@/lib/http';

export const labelApi = {
  // 查询标签
  queryFileList: data => {
    return postAxios('/tag/query', data);
  },
  // 查询标签
  tagAdd: data => {
    return postAxios('/tag/add', data);
  },
  // 更新标签
  tagUpdate: data => {
    return postAxios('/tag/update', data);
  },
  // 查询分类
  queryTagType: data => {
    return postAxios('/tagtype/query', data);
  },
  // 批量删除标签
  batchDeleteTag: (idList) => {
    return postAxios('/tag/batchDelete', {"idArray":idList});
  },
  //类型管理列表
  typeList:()=>{
    return postAxios('/category/query');
  },
  //添加类型
  addType: (data) => {
    return postAxios('/category/add', data);
  },
  //编辑类型
  editType: (data) => {
    return postAxios('/category/update', data);
  },
  //删除类型
  deleteType: (id) => {
    return postAxios('/category/delete/'+id);
  },
  
};
