import { postAxios, getAxios, postDownloadAxios } from '@/lib/http';

export const teacherApi = {
    // 分页查询课程 @author 卓大
    queryTeachers: (data) => {
        return postAxios('/teacher/page/query', data);
    },
    // 批量删除课程 @author 卓大
    batchDeleteTeachers: (idList) => {
        return postAxios('/teacher/deleteByIds', idList);
    },
    // 修改课程  @author 卓大
    updateTeacher: (data) => {
        return postAxios('/teacher/update',data);
    },
    // 添加课程
    addTeacher: (data) => {
        return postAxios('/teacher/add', data);
    },
};