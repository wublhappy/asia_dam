package com.gap.dam.module.business.tag.domain.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.gap.dam.common.base.BaseEntity;
import lombok.Data;

@Data
@TableName("t_tag")
public class TagEntity extends BaseEntity {
    /**
     * 标签名称
     */
    private String tagName;

    /**
     * 标签类型ID
     */
    private Long tagTypeId;

    /**
     * 标签类型名称
     */
    @TableField(exist = false)
    private String tagTypeName;

    /**
     * 标签描述
     */
    private String tagDesc;

}
