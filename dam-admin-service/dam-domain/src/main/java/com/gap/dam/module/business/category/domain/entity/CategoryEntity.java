package com.gap.dam.module.business.category.domain.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.gap.dam.common.base.BaseEntity;
import lombok.Data;

@Data
@TableName("t_category")
public class CategoryEntity extends BaseEntity {

    /**
     * 父节点ID
     */
    private Long parentId;

    /**
     * 类别名称
     */
    private String categoryName;

    /**
     * 排序
     */
    private Integer sort;

    /**
     * 文件ID
     */
    private Long fileId;

    private String pictureUrl;

    /**
     * 该字段不做映射
     */
    @TableField(exist = false)
    private String fileName;

    /**
     * 该字段不做映射
     */
    @TableField(exist = false)
    private String filePath;
}
