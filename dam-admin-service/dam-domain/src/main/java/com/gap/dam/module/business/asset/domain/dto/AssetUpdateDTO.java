package com.gap.dam.module.business.asset.domain.dto;

import lombok.Data;

@Data
public class AssetUpdateDTO extends AssetAddDTO {

    private Long id;
}
