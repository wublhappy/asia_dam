package com.gap.dam.module.business.tag.domain.vo;

import lombok.Data;

import java.util.Date;

@Data
public class TagTypeVO {

    private Long id;

    private String tagTypeName;

    private Date updateTime;

    private Date createTime;
}
