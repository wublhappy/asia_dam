package com.gap.dam.module.business.tag.domain.constant;

import com.gap.dam.constant.ResponseCodeConst;

public class TagResponseCodeConst extends ResponseCodeConst {

    public static final TagResponseCodeConst TAG_NAME_EMPTY = new TagResponseCodeConst(11001, "Tag name is empty！");

    public static final TagResponseCodeConst TAG_TYPE_EMPTY = new TagResponseCodeConst(11002, "Tag type is empty！");

    public static final TagResponseCodeConst TAG_LIST_EMPTY = new TagResponseCodeConst(11003, "标签列表为空");


    public TagResponseCodeConst(int code, String msg) {
        super(code, msg);
    }
}
