package com.gap.dam.module.business.filemanage.domain.constant;

import com.gap.dam.common.base.BaseEnum;

public enum  FileTypeEnum implements BaseEnum {

    FILE(0, "文件"),

    FOLDER(1, "文件夹");


    private int value;

    private String desc;


    FileTypeEnum(int value, String desc) {
        this.value = value;
        this.desc = desc;
    }

    @Override
    public Object getValue() {
        return this.value;
    }

    @Override
    public String getDesc() {
        return this.desc;
    }
}
