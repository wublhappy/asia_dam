package com.gap.dam.module.business.filemanage.domain.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class FolderAddDTO {
    @ApiModelProperty(value = "文件夹名称")
    private String folderName;

    @ApiModelProperty(value = "父文件夹ID，根目录为0")
    private Long parentFolderId;
}
