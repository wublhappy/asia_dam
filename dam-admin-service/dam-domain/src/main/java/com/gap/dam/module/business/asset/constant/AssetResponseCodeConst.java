package com.gap.dam.module.business.asset.constant;

import com.gap.dam.constant.ResponseCodeConst;

public class AssetResponseCodeConst extends ResponseCodeConst {

    /**
     * Asset不存在
     */
    public static final AssetResponseCodeConst ASSET_NOT_EXISTS = new AssetResponseCodeConst(9001, "Asset not exists！");

    public static final AssetResponseCodeConst ASSET_ID_LIST_EMPTY = new AssetResponseCodeConst(9002, "Asset列表为空");

    public static final AssetResponseCodeConst ASSET_TAG_LIST_EMPTY = new AssetResponseCodeConst(9003, "标签列表为空");

    public AssetResponseCodeConst(int code, String msg) {
        super(code, msg);
    }
}
