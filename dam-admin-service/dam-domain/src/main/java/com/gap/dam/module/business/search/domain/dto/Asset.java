package com.gap.dam.module.business.search.domain.dto;

import com.gap.dam.constant.ElasticSearchConst;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Document(indexName = ElasticSearchConst.INDEX_NAME, type="docs", shards = 1, replicas = 1)
public class Asset implements Serializable {
    private static final long serialVersionUID = 1203429678707108237L;

    @Id
    private String id;

    /**
     * 名称
     */
    @Field(type = FieldType.Text, analyzer = "ik_max_word")
    private String name;

    /**
     * URL
     */
    @Field(type = FieldType.Keyword, index = false)
    private String url;

    /**
     * 标签
     */
    @Field(type = FieldType.Keyword)
    private List<String> tags;


    /**
     * category类目
     */
    @Field(type = FieldType.Keyword)
    private String category;

    /**
     * Category Id
     */
    private Integer categoryId;

    /**
     * Category Picture Url
     */
    private String categoryPictureUrl;

    /**
     * Parent Category Id;
     */
    private Integer parentCategoryId;

    /**
     * All Parent Category id list
     */
    private List<Long> parentCategoryIdList;

    /**
     * 关键字名
     */
    private String keywordName;

    /**
     * 更新时间
     */
    @Field(type = FieldType.Date)
    private Date updateTime;

    /**
     * 创建时间
     */
    @Field(type = FieldType.Date)
    private Date createTime;

    /**
     * 文件类型
     */
    private String fileType;


    public void appendTag(String oneTag) {
        if (tags == null)
            tags = new ArrayList<>();
        tags.add(oneTag);
    }
}
