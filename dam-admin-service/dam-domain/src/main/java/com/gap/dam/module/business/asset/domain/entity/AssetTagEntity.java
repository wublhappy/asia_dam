package com.gap.dam.module.business.asset.domain.entity;

import com.gap.dam.common.base.BaseEntity;
import lombok.Data;

@Data
public class AssetTagEntity extends BaseEntity {

    private Long tagId;

    private Long assetId;


}
