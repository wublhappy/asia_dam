package com.gap.dam.module.business.asset.domain.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class AssetVO {

    @ApiModelProperty(value = "ID")
    private Long id;

    @ApiModelProperty(value = "文件名")
    private String name;

    @ApiModelProperty(value = "文件URL")
    private String url;

    @ApiModelProperty(value = "映射状态")
    private String mappingStatus;

    @ApiModelProperty(value = "发布状态")
    private String publishStatus;

    @ApiModelProperty(value = "Category ID")
    private Long categoryId;

    @ApiModelProperty(value = "父文件夹ID")
    private Long parentFolderId;

    @ApiModelProperty(value = "文件夹")
    private String folder;

    @ApiModelProperty(value = "映射文件")
    private String mappingFile;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date updateTime;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date uploaded;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date captureDate;

    private String fileSize;

    private String dimensions;

    private String colorSpace;

    private String fileType;

    private String usageRestrictions;

    private String regionalUsage;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date usageStartDate;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date usageExpiration;

    private String status;

    private String fileFormat;

    private String assetType;

    private String subBrand;

    private String season;

    private String year;

    private String campaignName;

    private String creator;

    private String retouchingVendor;

    private Integer retouchingProjectNumber;

    private String gapDepartment;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date dateCreated;

    @ApiModelProperty(value = "数据来源")
    private String source;

    @ApiModelProperty(value = "IP")
    private String ip;

    @ApiModelProperty(value = "Style Number")
    private String styleNumber;

    @ApiModelProperty(value = "CC Number")
    private String ccNumber;

    @ApiModelProperty(value = "Collection")
    private String collection;

    @ApiModelProperty(value = "KOL")
    private String kol;

    @ApiModelProperty(value = "Model")
    private String model;

    @ApiModelProperty(value = "Model Figure")
    private String modelFigure;

    @ApiModelProperty(value = "Model BWH")
    private String modelBwh;

    @ApiModelProperty(value = "Belonging")
    private String belonging;
}
