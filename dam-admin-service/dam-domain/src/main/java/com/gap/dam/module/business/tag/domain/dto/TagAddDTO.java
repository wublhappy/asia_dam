package com.gap.dam.module.business.tag.domain.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Data
public class TagAddDTO {
    @ApiModelProperty(value = "标签名称")
    @Length(min = 1, max = 30, message = "请输入正确的标签名称(1-30个字符)")
    @NotNull(message = "请输入正确的标签名称(1-30个字符)")
    private String tagName;

    @ApiModelProperty(value = "标签类型ID")
    @NotNull(message = "标签类型ID不能为空")
    private Integer tagTypeId;

    @ApiModelProperty(value = "标签描述")
    private String tagDesc;
}
