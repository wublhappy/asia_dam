package com.gap.dam.module.business.asset.domain.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
public class AssetTagDTO {

    @ApiModelProperty(value = "标签ID列表")
    private List<Long> tagIdList;

    @ApiModelProperty(value = "Asset ID列表")
    private List<Long> assetIdList;
}
