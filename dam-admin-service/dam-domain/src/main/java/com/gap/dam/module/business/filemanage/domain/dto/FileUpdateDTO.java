package com.gap.dam.module.business.filemanage.domain.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class FileUpdateDTO extends FolderAddDTO {
    @ApiModelProperty(value = "文件目录ID")
    private Long id;
}
