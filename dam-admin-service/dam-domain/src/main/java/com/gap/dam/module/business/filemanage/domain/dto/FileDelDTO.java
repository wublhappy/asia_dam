package com.gap.dam.module.business.filemanage.domain.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class FileDelDTO {

    @ApiModelProperty(value = "删除的文件或文件夹ID")
    private Long fileId;

    @ApiModelProperty(value = "文件类型 0 - 文件  1 - 文件夹")
    private Integer fileType;
}
