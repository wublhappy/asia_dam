package com.gap.dam.module.business.category.domain.vo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
public class CategoryVO {

    @ApiModelProperty(value = "Category id")
    private Long id;

    @ApiModelProperty(value = "Category 父Id")
    private Long parentId;

    // parent属性忽略json序列号，会引起循环引用
    @JsonIgnore
    private CategoryVO parent;

    @ApiModelProperty(value = "Category名称")
    private String categoryName;

    @ApiModelProperty(value = "Category排序字段")
    private Integer sort;

    @ApiModelProperty(value = "Category同级排在前面的id")
    private Long preId;

    @ApiModelProperty(value = "Category同级排在后面的id")
    private Long nextId;

    @ApiModelProperty(value = "上层Category名称")
    private String parentName;

    @ApiModelProperty(value = "Category子节点列表")
    private List<CategoryVO> children;

    @ApiModelProperty(value = "Category背景图片")
    private String pictureUrl;
}
