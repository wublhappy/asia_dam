package com.gap.dam.module.business.category.domain.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class CategoryDTO {
    private Long id;

    @ApiModelProperty("Category父ID")
    private Long parentId;

    @ApiModelProperty("Category名字")
    private String categoryName;

    private CategoryDTO parent;

    /**
     * 排序
     */
    private Integer sort;

    private Long fileId;

    private String fileName;

    private String filePath;
}
