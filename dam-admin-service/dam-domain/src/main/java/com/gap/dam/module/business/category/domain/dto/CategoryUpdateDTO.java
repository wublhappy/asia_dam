package com.gap.dam.module.business.category.domain.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class CategoryUpdateDTO extends CategoryAddDTO {

    @ApiModelProperty("ID")
    @NotNull(message = "ID不能为空")
    private Long id;
}
