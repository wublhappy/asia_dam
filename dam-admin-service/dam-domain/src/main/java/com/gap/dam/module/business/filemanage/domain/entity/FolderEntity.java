package com.gap.dam.module.business.filemanage.domain.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.gap.dam.common.base.BaseEntity;
import lombok.Data;

@Data
@TableName("t_asset_folder")
public class FolderEntity extends BaseEntity {

    private String folderName;

    private Long parentFolderId;
}
