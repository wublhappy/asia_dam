package com.gap.dam.module.business.category.domain.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Data
public class CategoryAddDTO {

    @ApiModelProperty("父Category Id")
    @NotNull(message = "Category父ID不能为空")
    private Long parentId;

    @ApiModelProperty("Category名称")
    @Length(min = 1, max = 20, message = "请输入正确Category名称(1-20个字符)")
    @NotNull(message = "请输入正确Category名称(1-20个字符)")
    private String categoryName;

    @ApiModelProperty("排序")
    private Integer sort;
}
