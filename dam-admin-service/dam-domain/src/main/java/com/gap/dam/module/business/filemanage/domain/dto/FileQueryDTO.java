package com.gap.dam.module.business.filemanage.domain.dto;

import com.gap.dam.common.base.PageParamDTO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class FileQueryDTO extends PageParamDTO {
    @ApiModelProperty(value = "文件ID")
    private Long id;

    @ApiModelProperty(value = "父文件夹ID")
    private Long parentFolderId;

    @ApiModelProperty(value = "文件夹名称")
    private String fileName;

    @ApiModelProperty(value = "文件类型 0-文件夹 1-文件")
    private Integer fileType;
}
