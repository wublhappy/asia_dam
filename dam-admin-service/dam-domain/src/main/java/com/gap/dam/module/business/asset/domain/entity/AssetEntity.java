package com.gap.dam.module.business.asset.domain.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.gap.dam.common.base.BaseEntity;
import lombok.Data;

import java.util.Date;

@Data
@TableName("t_asset")
public class AssetEntity extends BaseEntity {
    private String name;

    private String url;

    private String mappingStatus;

    private String publishStatus;

    private Long categoryId;

    private Long fileId;

    private Long parentFolderId;

    private String folder;

    private String mappingFile;

    private Date createTime;

    private Date updateTime;

    private Date uploaded;

    private Date captureDate;

    private String fileSize;

    private String dimensions;

    private String colorSpace;

    private String fileType;

    private String usageRestrictions;

    private String regionalUsage;

    private Date usageStartDate;

    private Date usageExpiration;

    private String status;

    private String fileFormat;

    private String assetType;

    private String subBrand;

    private String season;

    private String year;

    private String campaignName;

    private String creator;

    private String retouchingVendor;

    private Integer retouchingProjectNumber;

    private String gapDepartment;

    private Date dateCreated;

    private int isDelete;

    private String source;

    private String ip;

    private String styleNumber;

    private String ccNumber;

    private String collection;

    private String kol;

    private String model;

    private String modelFigure;

    private String modelBwh;

    private String belonging;

    private String fileClass;
}
