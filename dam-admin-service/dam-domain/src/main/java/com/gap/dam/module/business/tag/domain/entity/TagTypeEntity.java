package com.gap.dam.module.business.tag.domain.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.gap.dam.common.base.BaseEntity;
import lombok.Data;

@Data
@TableName("t_tag_type")
public class TagTypeEntity extends BaseEntity {

    /**
     * 标签类型名称
     */
    private String tagTypeName;
}
