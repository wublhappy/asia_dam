package com.gap.dam.module.business.tag.domain.dto;

import com.gap.dam.common.base.PageParamDTO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class TagQueryDTO extends PageParamDTO  {

    @ApiModelProperty(value = "标签名称")
    private String tagName;

    @ApiModelProperty(value = "标签类型ID")
    private String tagTypeId;

}
