package com.gap.dam.module.business.filemanage.domain.constant;

import com.gap.dam.constant.ResponseCodeConst;

public class FileManageResponseCodeConst extends ResponseCodeConst {


    public static final FileManageResponseCodeConst PARENT_FOLDER_NOTEXISTS = new FileManageResponseCodeConst(12001, "父目录不存在");

    public static final FileManageResponseCodeConst FILE_TYPE_INVALID = new FileManageResponseCodeConst(12002, "文件类型不正确");

    public static final FileManageResponseCodeConst FILE_NOT_EXISTS = new FileManageResponseCodeConst(12003, "文件不存在");

    public static final FileManageResponseCodeConst FOLDER_NOT_EXISTS = new FileManageResponseCodeConst(12004, "文件目录不存在");

    public static final FileManageResponseCodeConst FOLDER_INVALID = new FileManageResponseCodeConst(12005, "文件目录无效");


    public FileManageResponseCodeConst(int code, String msg) {
        super(code, msg);
    }
}
