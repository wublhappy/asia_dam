package com.gap.dam.module.business.filemanage.domain.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class FileDTO {

    @ApiModelProperty(value = "文件ID")
    private Long id;

    @ApiModelProperty(value = "父文件目录ID")
    private Long parentFolderId;

    @ApiModelProperty(value = "文件类型 0 - 文件  1 - 文件夹")
    private Integer fileType;

    @ApiModelProperty(value = "文件名")
    private String fileName;
}
