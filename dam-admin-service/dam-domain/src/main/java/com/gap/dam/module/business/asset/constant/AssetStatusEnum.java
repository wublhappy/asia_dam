package com.gap.dam.module.business.asset.constant;

import com.gap.dam.common.base.BaseEnum;

public enum AssetStatusEnum implements BaseEnum {

    PUBLISH(1, "已发布"),
    UNPUBLISH(0, "未发布");


    AssetStatusEnum(int value, String desc) {
        this.value = value;
        this.desc = desc;
    }

    private int value;

    private String desc;

    @Override
    public Object getValue() {
        return value;
    }

    @Override
    public String getDesc() {
        return desc;
    }
}
