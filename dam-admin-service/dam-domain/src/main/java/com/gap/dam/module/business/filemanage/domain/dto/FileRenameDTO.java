package com.gap.dam.module.business.filemanage.domain.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class FileRenameDTO {

    @ApiModelProperty(value = "重命名文件或文件夹ID")
    private Long fileId;

    @ApiModelProperty(value = "文件新名称")
    private String fileNewName;

    @ApiModelProperty(value = "文件类型 0 - 文件  1 - 文件夹")
    private Integer fileType;
}
