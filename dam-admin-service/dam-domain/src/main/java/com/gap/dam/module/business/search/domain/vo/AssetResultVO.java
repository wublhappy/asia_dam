package com.gap.dam.module.business.search.domain.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class AssetResultVO {

    @ApiModelProperty(value = "ID")
    private String id;

    @ApiModelProperty(value = "Asset名称")
    private String name;

    @ApiModelProperty(value = "Asset Category")
    private String category;

    @ApiModelProperty(value = "Asset Category ID")
    private Integer categoryId;

    @ApiModelProperty(value = "Asset Category父ID")
    private Integer parentCategoryId;

    @ApiModelProperty(value = "Asset Category 所有父节点的列表")
    private List<Long> parentCategoryIdList;

    @ApiModelProperty(value = "Asset URL")
    private String url;

    @ApiModelProperty(value = "Asset picture URL（用于文件夹背景图）")
    private String pictureUrl;

    @ApiModelProperty(value = "创建时间")
    private Date createTime;

    @ApiModelProperty(value = "更新时间")
    private Date updateTime;

    @ApiModelProperty(value = "文件类型: category, jpeg, jpg, audio, video")
    private String fileType;
}
