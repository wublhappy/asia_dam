package com.gap.dam.module.support.file.constant;


import com.gap.dam.common.base.BaseEnum;

/**
 * []
 *
 * @author yandanyang
 * @version 1.0
 * @since JDK1.8
 */
public enum FileModuleTypeEnum implements BaseEnum {

    /**
     * path 首字符不能包含\ 或者/
     */

    BACK_USER(1, "backUser/config", "backUser"),

    CODE_REVIEW(2, "codeReview", "CodeReview"),

    ASSET(3, "asset", "Asset Path"),

    ASSET_TEMPLATE(4, "template", "Asset Template Path");

    private Integer value;

    private String path;

    private String desc;

    FileModuleTypeEnum(Integer value, String path, String desc) {
        this.value = value;
        this.path = path;
        this.desc = desc;
    }

    public String getPath() {
        return path;
    }

    @Override
    public Integer getValue() {
        return this.value;
    }

    @Override
    public String getDesc() {
        return this.desc;
    }
}
