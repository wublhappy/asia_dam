package com.gap.dam.module.business.search.domain.dto;

import com.gap.dam.common.base.PageParamDTO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
public class AssetSearchDTO extends PageParamDTO {

    /**
     * Asset 名称
     */
    @ApiModelProperty(value = "Asset名称")
    private String name;

    /**
     * Asset标签
     */
    @ApiModelProperty(value = "Asset标签")
    private List<String> tags;

    /**
     * Asset目录
     */
    @ApiModelProperty(value = "Asset分类")
    private String category;
}
