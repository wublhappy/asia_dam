package com.gap.dam.module.business.asset.domain.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

@Data
public class AssetDTO {
    private Long id;

    private String name;

    private String url;

    private String mappingStatus;

    private String publishStatus;

    private Long categoryId;

    private Long parentFolderId;

    private String folder;

    private String mappingFile;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date updateTime;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date uploaded;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date captureDate;

    private String fileSize;

    private String dimensions;

    private String colorSpace;

    private String fileType;

    private String usageRestrictions;

    private String regionalUsage;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date usageStartDate;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date usageExpiration;

    private String status;

    private String fileFormat;

    private String assetType;

    private String subBrand;

    private String season;

    private String year;

    private String campaignName;

    private String creator;

    private String retouchingVendor;

    private Integer retouchingProjectNumber;

    private String gapDepartment;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date dateCreated;
}
