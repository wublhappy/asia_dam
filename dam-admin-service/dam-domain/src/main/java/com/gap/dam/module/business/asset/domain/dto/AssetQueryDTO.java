package com.gap.dam.module.business.asset.domain.dto;

import com.gap.dam.common.base.PageParamDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
public class AssetQueryDTO extends PageParamDTO {

    @ApiModelProperty(value = "审核状态 0-未审核 1-已审核")
    private Integer publishStatus;

    @ApiModelProperty(value = "标签列表")
    private List<String> tags;

    @ApiModelProperty(value = "Asset名称")
    private String name;

    @ApiModelProperty(value = "Category ID列表")
    private List<Long> categoryIdList;
}
