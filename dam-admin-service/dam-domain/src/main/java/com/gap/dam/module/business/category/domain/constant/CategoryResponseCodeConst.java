package com.gap.dam.module.business.category.domain.constant;

import com.gap.dam.constant.ResponseCodeConst;

public class CategoryResponseCodeConst extends ResponseCodeConst  {
    public static final CategoryResponseCodeConst CATEGORY_NOT_EXISTS = new CategoryResponseCodeConst(10001, "Category not exists！");


    public CategoryResponseCodeConst(int code, String msg) {
        super(code, msg);
    }
}
