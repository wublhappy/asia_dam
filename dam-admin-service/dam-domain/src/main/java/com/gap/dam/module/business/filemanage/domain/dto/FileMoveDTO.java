package com.gap.dam.module.business.filemanage.domain.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
public class FileMoveDTO {

    @ApiModelProperty(value = "移动的原文件列表(文件或者文件夹)")
    private List<FileDTO> sourceFileList;

    @ApiModelProperty(value = "目标文件夹ID")
    private Long destFolderId;
}
