package com.gap.dam;

import com.github.javafaker.Faker;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.jetbrains.annotations.NotNull;

import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Data
public class HouseInfo {
    private Long id;

    private String title;

    private Double price;


    public HouseInfo(Long id, String title, Double price) {
        this.id = id;
        this.title = title;
        this.price = price;
    }

    public static void main(String[] args) {
        Faker faker = new Faker(Locale.CHINA);

        List<HouseInfo> houseInfoList = Stream.generate(
                () -> new HouseInfo(faker.number().randomNumber(), faker.name().title(), faker.number().randomDouble(5000, 1000, 5000))).collect(Collectors.toList());
        houseInfoList.forEach(System.out::println);
    }
}
