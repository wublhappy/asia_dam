package com.gap.dam;

import com.github.javafaker.Faker;
import org.apache.http.HttpHost;
import org.elasticsearch.action.delete.DeleteRequest;
import org.elasticsearch.action.delete.DeleteResponse;
import org.elasticsearch.action.get.GetRequest;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.action.update.UpdateResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.fetch.subphase.FetchSourceContext;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TestHighES {

    private RestHighLevelClient client;

    @Before
    public void init() {
        this.client = new RestHighLevelClient(
                RestClient.builder(
                        new HttpHost("47.98.172.32", 9200, "http")));
    }

    @After
    public void after() {
        try {
            this.client.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testCreate() throws Exception {
        Faker faker = new Faker(Locale.CHINA);

        List<HouseInfo> houseInfoList = Stream.generate(
                () -> new HouseInfo(faker.number().randomNumber(), faker.name().title(), faker.number().randomDouble(2, 1, 100))).limit(100).collect(Collectors.toList());
        for (HouseInfo houseInfo : houseInfoList) {
            Map<String, Object> jsonMap = new HashMap<>();
            jsonMap.put("id", houseInfo.getId());
            jsonMap.put("title", houseInfo.getTitle());
            jsonMap.put("price", houseInfo.getPrice());
            IndexRequest indexRequest = new IndexRequest("lianjia").source(jsonMap);
            IndexResponse indexResponse = this.client.index(indexRequest, RequestOptions.DEFAULT);
            System.out.println("result->" + indexResponse.getResult());
            System.out.println("id->" + indexResponse.getId());
            System.out.println("index->" + indexResponse.getIndex());
            System.out.println("shardinfo->" + indexResponse.getShardInfo());
        }
    }

    @Test
    public void testExists() throws IOException {
        GetRequest getRequest = new GetRequest("lianjia", "A7sQi3oBDEUmaMNe1g2-");
        getRequest.fetchSourceContext(new FetchSourceContext(false));
        boolean isExists = this.client.exists(getRequest, RequestOptions.DEFAULT);
        System.out.println(isExists);
    }

    @Test
    public void testDelete() throws IOException {
        DeleteRequest deleteRequest = new DeleteRequest(
                "lianjia", "A7sQi3oBDEUmaMNe1g2-");
        DeleteResponse deleteResponse = this.client.delete(deleteRequest, RequestOptions.DEFAULT);
        System.out.println(deleteResponse.status());
    }

    @Test
    public void testUpdate() throws IOException {
        UpdateRequest updateRequest = new UpdateRequest("lianjia", "Dbtai3oBDEUmaMNe3A09");
        Map<String, Object> data = new HashMap<>();
        data.put("title", "袁科教育办公室");
        data.put("price", "12000");
        updateRequest.doc(data);

        UpdateResponse updateResponse = this.client.update(updateRequest, RequestOptions.DEFAULT);
        System.out.println("version---" + updateResponse.getVersion());
    }

    @Test
    public void testQuery() throws IOException {
        SearchRequest searchRequest = new SearchRequest();
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
//        searchSourceBuilder.query(QueryBuilders.matchAllQuery());
        searchSourceBuilder.from(0);
        searchSourceBuilder.size(5);
        searchSourceBuilder.timeout(new TimeValue(60, TimeUnit.MILLISECONDS));
        searchRequest.source(searchSourceBuilder);

        SearchResponse searchResponse = this.client.search(searchRequest, RequestOptions.DEFAULT);
        System.out.println("搜索到" + searchResponse.getHits().getTotalHits() + "条");
        SearchHits searchHits = searchResponse.getHits();
        for (SearchHit searchHit : searchHits) {
            System.out.println(searchHit.getSourceAsString());
        }
    }
}
