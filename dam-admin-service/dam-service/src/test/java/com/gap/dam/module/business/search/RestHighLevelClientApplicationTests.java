package com.gap.dam.module.business.search;

import com.gap.dam.constant.ElasticSearchConst;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

//@RunWith(SpringRunner.class)
//@SpringBootTest(classes = RestHighLevelClientApplicationTests.class)
public class RestHighLevelClientApplicationTests {

    private AssetSearchService assetSearchService;

    @Before
    public void startup() {
        assetSearchService = new AssetSearchService();
    }

    @Test
    public void contextLoads() {
        System.out.println(111);
    }

    @Test
    public void createIndex() {
        assetSearchService.createIndex(ElasticSearchConst.INDEX_NAME);
    }
}
