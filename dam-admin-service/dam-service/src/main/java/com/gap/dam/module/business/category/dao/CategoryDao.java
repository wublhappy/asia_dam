package com.gap.dam.module.business.category.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gap.dam.module.business.category.domain.entity.CategoryEntity;
import com.gap.dam.module.business.category.domain.vo.CategoryVO;
import org.mapstruct.Mapper;
import org.springframework.stereotype.Component;

import java.util.List;

@Mapper
@Component
public interface CategoryDao extends BaseMapper<CategoryEntity> {

    List<CategoryEntity> queryAll();
}
