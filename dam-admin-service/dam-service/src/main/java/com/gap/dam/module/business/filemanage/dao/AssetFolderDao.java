package com.gap.dam.module.business.filemanage.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gap.dam.module.business.filemanage.domain.dto.FileUpdateDTO;
import com.gap.dam.module.business.filemanage.domain.dto.FileQueryDTO;
import com.gap.dam.module.business.filemanage.domain.entity.FolderEntity;
import com.gap.dam.module.business.filemanage.domain.vo.FileVO;
import org.apache.ibatis.annotations.Param;
import org.mapstruct.Mapper;
import org.springframework.stereotype.Component;

import java.util.List;

@Mapper
@Component
public interface AssetFolderDao extends BaseMapper<FolderEntity> {

    /**
     * 不带分页查询目录
     * @param queryDTO
     * @return
     */
    List<FolderEntity> queryFolderList(@Param("queryDTO") FileQueryDTO queryDTO);

    /**
     * 批量更新多个
     * @param fileUpdateDTO
     * @param idList
     */
    void batchUpdate(@Param("updateDTO") FileUpdateDTO fileUpdateDTO, @Param("idList") List<Long> idList);

    /**
     * 分页查询文件列表
     * @param page
     * @param queryDTO
     * @return
     */
    List<FileVO> queryFileList(Page page, @Param("queryDTO") FileQueryDTO queryDTO);
}
