package com.gap.dam.module.business.tag;

import com.gap.dam.common.base.ResponseDTO;
import com.gap.dam.module.business.tag.dao.TagTypeDao;
import com.gap.dam.module.business.tag.domain.entity.TagTypeEntity;
import com.gap.dam.module.business.tag.domain.vo.TagTypeVO;
import com.gap.dam.util.BeanUtil;
import org.apache.commons.compress.utils.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TagTypeService {

    @Autowired
    private TagTypeDao tagTypeDao;


    /**
     * 查询所有标签类型
     * @return
     */
    public ResponseDTO<List<TagTypeVO>> queryAll() {
        List<TagTypeEntity> tagTypeEntityList = Lists.newArrayList();
        return ResponseDTO.succData(BeanUtil.copyList(tagTypeEntityList, TagTypeVO.class));
    }
}
