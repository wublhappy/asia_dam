package com.gap.dam.config;

import org.jetbrains.annotations.NotNull;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
@ConfigurationProperties(prefix = "dam.data.elasticsearch")
public class ElasticSearchProperties {

    private String schema = "http";

    /**
     * 集群名称
     */
    private String clusterName = "elasticsearch";

    /**
     * 集群的节点
     */
    @NotNull
    private List<String> clusterNodes = new ArrayList<>();

    /**
     * 连接超时时间（毫秒）
     */
    private Integer connectionTimeout = 1000;

    /**
     * socket超时时间
     */
    private Integer socketTimeout = 3000;

    /**
     * 连接请求超时时间（毫秒）
     */
    private Integer connectionRequestTimeout = 500;

    /**
     * 每个路由最大连接数
     */
    private Integer maxConnectionPerRoute = 10;

    /**
     * 最大连接数
     */
    private Integer maxConnectionTotal = 30;

    /**
     * 最大配置信息
     */
    private Index index = new Index();

    /**
     * 认证账户
     */
    private Account account = new Account();

    /**
     * 配置索引信息
     */
    public static class Index {
        /**
         * 分片数量
         */
        private Integer numberOfShards = 1;

        /**
         * 副本数量
         */
        private Integer numberOfReplicas = 1;


        public Integer getNumberOfShards() {
            return numberOfShards;
        }

        public void setNumberOfShards(Integer numberOfShards) {
            this.numberOfShards = numberOfShards;
        }

        public Integer getNumberOfReplicas() {
            return numberOfReplicas;
        }

        public void setNumberOfReplicas(Integer numberOfReplicas) {
            this.numberOfReplicas = numberOfReplicas;
        }
    }

    /**
     * 认证账号
     */
    public static class Account {
        /**
         * 认证用户
         */
        private String userName;

        /**
         * 认证密码
         */
        private String password;

        public String getUserName() {
            return userName;
        }

        public void setUserName(String userName) {
            this.userName = userName;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }
    }

    public String getSchema() {
        return schema;
    }

    public void setSchema(String schema) {
        this.schema = schema;
    }

    public String getClusterName() {
        return clusterName;
    }

    public void setClusterName(String clusterName) {
        this.clusterName = clusterName;
    }

    @NotNull
    public List<String> getClusterNodes() {
        return clusterNodes;
    }

    public void setClusterNodes(@NotNull List<String> clusterNodes) {
        this.clusterNodes = clusterNodes;
    }

    public Integer getConnectionTimeout() {
        return connectionTimeout;
    }

    public void setConnectionTimeout(Integer connectionTimeout) {
        this.connectionTimeout = connectionTimeout;
    }

    public Integer getSocketTimeout() {
        return socketTimeout;
    }

    public void setSocketTimeout(Integer socketTimeout) {
        this.socketTimeout = socketTimeout;
    }

    public Integer getConnectionRequestTimeout() {
        return connectionRequestTimeout;
    }

    public void setConnectionRequestTimeout(Integer connectionRequestTimeout) {
        this.connectionRequestTimeout = connectionRequestTimeout;
    }

    public Integer getMaxConnectionPerRoute() {
        return maxConnectionPerRoute;
    }

    public void setMaxConnectionPerRoute(Integer maxConnectionPerRoute) {
        this.maxConnectionPerRoute = maxConnectionPerRoute;
    }

    public Integer getMaxConnectionTotal() {
        return maxConnectionTotal;
    }

    public void setMaxConnectionTotal(Integer maxConnectionTotal) {
        this.maxConnectionTotal = maxConnectionTotal;
    }

    public Index getIndex() {
        return index;
    }

    public void setIndex(Index index) {
        this.index = index;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }
}
