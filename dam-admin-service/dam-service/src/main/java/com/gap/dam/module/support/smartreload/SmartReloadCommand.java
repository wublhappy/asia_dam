package com.gap.dam.module.support.smartreload;

import com.gap.dam.common.reload.abstracts.AbstractReloadCommand4Spring;
import com.gap.dam.common.reload.domain.entity.ReloadItem;
import com.gap.dam.common.reload.domain.entity.ReloadResult;
import com.gap.dam.module.support.smartreload.dao.ReloadItemDao;
import com.gap.dam.module.support.smartreload.dao.ReloadResultDao;
import com.gap.dam.module.support.smartreload.domain.entity.ReloadItemEntity;
import com.gap.dam.module.support.smartreload.domain.entity.ReloadResultEntity;
import com.gap.dam.util.BeanUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Smart Reload 业务
 *
 * @author listen
 * @date 2018/02/10 09:18
 */
@Component
public class SmartReloadCommand extends AbstractReloadCommand4Spring {

    @Autowired
    private ReloadItemDao reloadItemDao;

    @Autowired
    private ReloadResultDao reloadResultDao;

    /**
     * 读取数据库中SmartReload项
     *
     * @return List<ReloadItem>
     */
    @Override
    public List<ReloadItem> readReloadItem() {
        List<ReloadItemEntity> reloadItemEntityList = reloadItemDao.selectList(null);
        return BeanUtil.copyList(reloadItemEntityList, ReloadItem.class);
    }

    /**
     * 保存reload结果
     *
     * @param smartReloadResult
     */
    @Override
    public void handleReloadResult(ReloadResult smartReloadResult) {
        ReloadResultEntity reloadResultEntity = BeanUtil.copy(smartReloadResult, ReloadResultEntity.class);
        reloadResultDao.insert(reloadResultEntity);
    }
}
