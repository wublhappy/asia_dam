package com.gap.dam.module.business.tag.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gap.dam.module.business.tag.domain.dto.TagQueryDTO;
import com.gap.dam.module.business.tag.domain.entity.TagEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.List;

@Mapper
@Component
public interface TagDao extends BaseMapper<TagEntity> {

    /**
     * 分页查询Tag
     * @param page
     * @param tagQueryDTO
     * @return
     */
    List<TagEntity> queryList(Page page, @Param("queryDTO") TagQueryDTO tagQueryDTO);

    /**
     * 按照ID查询
     * @param id
     * @return
     */
    TagEntity queryById(Long id);

    /**
     * 根据Asset Id列表查询标签
     * @param assetIdList
     * @return
     */
    List<TagEntity> queryTagsByAssetIdList(@Param("idList") List<Long> assetIdList);
}
