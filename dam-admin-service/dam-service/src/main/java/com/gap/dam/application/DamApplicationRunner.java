package com.gap.dam.application;

import com.gap.dam.constant.ResponseCodeConst;
import com.gap.dam.module.business.log.orderoperatelog.constant.OrderOperateLogOperateTypeConst;
import com.gap.dam.module.support.file.constant.FileResponseCodeConst;
import com.gap.dam.module.system.department.DepartmentResponseCodeConst;
import com.gap.dam.module.system.employee.constant.EmployeeResponseCodeConst;
import com.gap.dam.module.system.login.LoginResponseCodeConst;
import com.gap.dam.module.system.privilege.constant.PrivilegeResponseCodeConst;
import com.gap.dam.module.system.role.basic.RoleResponseCodeConst;
import com.gap.dam.module.system.systemconfig.constant.SystemConfigResponseCodeConst;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

/**
 * @program: gap-webdam
 * @description:
 * @author: Bing Li Wu
 * @create: 2021/7/6 0:23
 **/
@Component
public class DamApplicationRunner implements ApplicationRunner {
    @Override
    public void run(ApplicationArguments args) throws Exception {
        // 系统功能，从0开始，step=1000
        ResponseCodeConst.ResponseCodeContainer.register(ResponseCodeConst.class, 0, 1000);
        ResponseCodeConst.ResponseCodeContainer.register(LoginResponseCodeConst.class, 1001, 1999);
        ResponseCodeConst.ResponseCodeContainer.register(DepartmentResponseCodeConst.class, 2001, 2999);
        ResponseCodeConst.ResponseCodeContainer.register(EmployeeResponseCodeConst.class, 3001, 3999);
        ResponseCodeConst.ResponseCodeContainer.register(FileResponseCodeConst.class, 4001, 4999);
        ResponseCodeConst.ResponseCodeContainer.register(SystemConfigResponseCodeConst.class, 5001, 5999);
        ResponseCodeConst.ResponseCodeContainer.register(RoleResponseCodeConst.class, 6001, 6999);
        ResponseCodeConst.ResponseCodeContainer.register(PrivilegeResponseCodeConst.class, 7001, 7999);
        ResponseCodeConst.ResponseCodeContainer.register(OrderOperateLogOperateTypeConst.class, 8001, 8999);
    }
}
