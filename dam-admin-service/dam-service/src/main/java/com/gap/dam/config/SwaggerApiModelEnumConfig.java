package com.gap.dam.config;

import com.gap.dam.common.swagger.SwaggerApiModelEnumPlugin;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.core.annotation.Order;
import springfox.documentation.swagger.common.SwaggerPluginSupport;

/**
 * [ 对于枚举类进行swagger注解，与前端的vue-enum相匹配 ]
 *
 */
@Configuration
@Profile({"dev", "sit", "pre", "prod"})
public class SwaggerApiModelEnumConfig {

    @Bean
    @Order(SwaggerPluginSupport.SWAGGER_PLUGIN_ORDER + 1)
    public SwaggerApiModelEnumPlugin swaggerEnum(){
        return new SwaggerApiModelEnumPlugin();
    }
}
