package com.gap.dam.module.business.asset.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gap.dam.module.business.asset.domain.entity.AssetTagEntity;
import org.apache.ibatis.annotations.Param;
import org.mapstruct.Mapper;
import org.springframework.stereotype.Component;

import java.util.List;

@Mapper
@Component
public interface AssetTagDao extends BaseMapper<AssetTagEntity> {

    void batchDeleteTagsByAssetIdList(@Param("assetIdList") List<Long> assetIdList);

    void batchInsertAssetTags(@Param("assetTagList") List<AssetTagEntity> assetTagList);
}
