package com.gap.dam.module.business.asset.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gap.dam.module.business.asset.domain.dto.AssetDTO;
import com.gap.dam.module.business.asset.domain.dto.AssetQueryDTO;
import com.gap.dam.module.business.asset.domain.dto.AssetUpdateDTO;
import com.gap.dam.module.business.asset.domain.entity.AssetEntity;
import com.gap.dam.module.business.asset.domain.vo.AssetVO;
import org.apache.ibatis.annotations.Param;
import org.mapstruct.Mapper;
import org.springframework.stereotype.Component;

import java.util.List;

@Mapper
@Component
public interface AssetDao extends BaseMapper<AssetEntity>  {

    /**
     * 分页查询Asset列表
     * @param page
     * @param queryDTO
     * @return
     */
    List<AssetEntity> queryList(Page page, @Param("queryDTO") AssetQueryDTO queryDTO);

    /**
     * 不分页查询Asset列表
     * @param queryDTO
     * @return
     */
    List<AssetEntity> queryList(@Param("queryDTO") AssetQueryDTO queryDTO);



    /**
     * 批量逻辑删除
     * @param assetIds
     * @param isDelete
     */
    void batchDeleteAsset(@Param("assetIds") List<Long> assetIds, @Param("isDelete") Integer isDelete);

    /**
     * 批量更新
     * @param assetIds
     * @param updateDTO
     */
    void batchUpdateAsset(@Param("assetIds") List<Long> assetIds, @Param("updateDTO") AssetUpdateDTO updateDTO);
}
