package com.gap.dam.config;

import com.gap.dam.common.reload.ReloadManager;
import com.gap.dam.common.reload.interfaces.ReloadThreadLogger;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * [  ]
 *
 */
@Slf4j
@Configuration
public class ReloadConfig {

    @Value("${smart-reload.thread-count}")
    private Integer threadCount;

    @Bean
    public ReloadManager initSmartReloadManager() {
        /**
         * 创建 Reload Manager 调度器
         */
        ReloadManager smartReloadManager = new ReloadManager(new ReloadThreadLogger() {
            @Override
            public void error(String string) {
                log.error(string);
            }

            @Override
            public void error(String string, Throwable e) {
                log.error(string, e);
            }
        }, threadCount);
        return smartReloadManager;
    }
}
