package com.gap.dam.module.business.tag;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gap.dam.common.base.PageResultDTO;
import com.gap.dam.common.base.ResponseDTO;
import com.gap.dam.module.business.tag.dao.TagDao;
import com.gap.dam.module.business.tag.domain.constant.TagResponseCodeConst;
import com.gap.dam.module.business.tag.domain.dto.TagAddDTO;
import com.gap.dam.module.business.tag.domain.dto.TagQueryDTO;
import com.gap.dam.module.business.tag.domain.dto.TagUpdateDTO;
import com.gap.dam.module.business.tag.domain.entity.TagEntity;
import com.gap.dam.module.business.tag.domain.vo.TagVO;
import com.gap.dam.util.BeanUtil;
import com.gap.dam.util.PageUtil;
import com.gap.dam.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class TagService {

    @Autowired
    private TagDao tagDao;


    /**
     * 分页查询Tag Page
     * @param queryDTO
     * @return
     */
    public ResponseDTO<PageResultDTO<TagVO>> queryTagList(TagQueryDTO queryDTO) {
        Page pageParam = PageUtil.convert2QueryPage(queryDTO);
        List<TagEntity> tagEntityList = tagDao.queryList(pageParam, queryDTO);
        PageResultDTO<TagVO> pageResultDTO = PageUtil.convert2PageResult(pageParam, tagEntityList, TagVO.class);
        return ResponseDTO.succData(pageResultDTO);
    }


    /**
     * 新增Tag
     * @param tagAddDTO
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public ResponseDTO<Long> addTag(TagAddDTO tagAddDTO) {
        if (StringUtil.isEmpty(tagAddDTO.getTagName())) {
            return ResponseDTO.wrap(TagResponseCodeConst.TAG_NAME_EMPTY);
        }
        if (tagAddDTO.getTagTypeId() == null) {
            return ResponseDTO.wrap(TagResponseCodeConst.TAG_TYPE_EMPTY);
        }
        TagEntity tagEntity = BeanUtil.copy(tagAddDTO, TagEntity.class);
        tagDao.insert(tagEntity);
        return ResponseDTO.succData(tagEntity.getId());
    }

    /**
     * 根据ID更新
     * @param tagUpdateDTO
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public ResponseDTO<String> updateTagById(TagUpdateDTO tagUpdateDTO) {
        if (StringUtil.isEmpty(tagUpdateDTO.getTagName())) {
            return ResponseDTO.wrap(TagResponseCodeConst.TAG_NAME_EMPTY);
        }
        if (tagUpdateDTO.getTagTypeId() == null) {
            return ResponseDTO.wrap(TagResponseCodeConst.TAG_TYPE_EMPTY);
        }
        TagEntity tagEntity = BeanUtil.copy(tagUpdateDTO, TagEntity.class);
        tagDao.updateById(tagEntity);
        return ResponseDTO.succ();
    }

    /**
     * 根据ID删除
     * @param id
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public ResponseDTO<String> deleteById(Long id) {
        tagDao.deleteById(id);
        return ResponseDTO.succ();
    }

    public ResponseDTO<String> deleteTagByIds(List<Long> tagIds) {
        return ResponseDTO.succ();
    }
}
