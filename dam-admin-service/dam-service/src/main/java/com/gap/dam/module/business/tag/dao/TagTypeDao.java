package com.gap.dam.module.business.tag.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gap.dam.module.business.tag.domain.entity.TagTypeEntity;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;

import java.util.List;

@Mapper
@Component
public interface TagTypeDao extends BaseMapper<TagTypeEntity> {

    List<TagTypeEntity> queryList();
}
