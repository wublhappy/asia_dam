package com.gap;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

/**
 * @program: gap-webdam
 * @description:
 * @author: Bing Li Wu
 * @create: 2021/7/4 7:54
 **/
@SpringBootApplication(scanBasePackages = {"com.gap.dam", "cn.afterturn.easypoi"})
@EnableAspectJAutoProxy(proxyTargetClass = true, exposeProxy = true)
public class DamFrontEndApplication {
    public static void main(String[] args) {
        SpringApplication.run(DamFrontEndApplication.class, args);
    }
}
