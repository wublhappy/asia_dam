package com.gap.dam.module.business.asset;

import com.gap.dam.common.anno.NoNeedLogin;
import com.gap.dam.common.anno.OperateLog;
import com.gap.dam.common.base.IdListQueryDTO;
import com.gap.dam.common.base.ResponseDTO;
import com.gap.dam.constant.SwaggerTagConst;
import com.gap.dam.module.business.asset.domain.dto.AssetQueryDTO;
import com.gap.dam.module.business.asset.domain.vo.AssetVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;

@RestController
@Api(tags = {SwaggerTagConst.SearchFrontEnd.SEARCH_SYSTEM_ASSEET})
@OperateLog
public class AssetController {

    @Autowired
    private AssetService assetService;

    @ApiOperation(value = "查询Asset列表", notes = "asset")
    @PostMapping("/asset/query/all")
    @NoNeedLogin
    public ResponseDTO<List<AssetVO>> queryAssetList(@Valid @RequestBody AssetQueryDTO queryDTO) {
        return assetService.queryAllAssetList(queryDTO);
    }

    @ApiOperation(value = "查询单个Asset详细信息", notes = "")
    @GetMapping("/asset/query/{assetId}")
    public ResponseDTO<AssetVO> queryAsset(@PathVariable("assetId") Long assetId) {
        return assetService.queryById(assetId);
    }

    @ApiOperation(value = "文件下载通用接口（流下载）")
    @GetMapping("/asset/file/downLoad")
    @NoNeedLogin
    public ResponseEntity<byte[]> downLoadById(Long id, HttpServletRequest request) {
        return assetService.downLoadById(id, request);
    }

    @ApiOperation(value = "多文件下载接口（流下载）")
    @PostMapping("/asset/file/multidownload")
    @NoNeedLogin
    public ResponseEntity<byte[]> downloadByIdList(@RequestBody IdListQueryDTO idList, HttpServletRequest request) {
        return assetService.downloadMultiByIds(idList.getIdArray(), request);
    }
}
