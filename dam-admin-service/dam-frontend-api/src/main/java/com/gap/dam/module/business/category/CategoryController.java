package com.gap.dam.module.business.category;

import com.gap.dam.common.anno.NoNeedLogin;
import com.gap.dam.common.anno.OperateLog;
import com.gap.dam.common.base.ResponseDTO;
import com.gap.dam.constant.SwaggerTagConst;
import com.gap.dam.module.business.category.domain.vo.CategoryVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@Api(tags = {SwaggerTagConst.SearchFrontEnd.SEARCH_SYSTEM_CATEGORY})
@OperateLog
public class CategoryController {

    @Autowired
    private CategoryService categoryService;

    @PostMapping("/category/query")
    @ApiOperation(value = "查询所有Category列表", notes = "category")
    @NoNeedLogin
    public ResponseDTO<List<CategoryVO>> queryCategoryList() {
        return categoryService.queryAll();
    }
}
