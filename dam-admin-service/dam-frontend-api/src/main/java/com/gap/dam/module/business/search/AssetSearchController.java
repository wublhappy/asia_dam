package com.gap.dam.module.business.search;

import com.gap.dam.common.anno.NoNeedLogin;
import com.gap.dam.common.anno.OperateLog;
import com.gap.dam.common.base.ResponseDTO;
import com.gap.dam.constant.SwaggerTagConst;
import com.gap.dam.module.business.search.domain.dto.AssetSearchDTO;
import com.gap.dam.module.business.search.domain.vo.AssetResultVO;
import com.gap.dam.util.ESSearchResultDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@Api(tags = {SwaggerTagConst.SearchFrontEnd.SEARCH_SYSTEM_ASSET_SEARCH})
@OperateLog
public class AssetSearchController {

    @Autowired
    private AssetSearchService searchService;

    @ApiOperation(value = "搜索Asset和Category", notes = "search")
    @PostMapping("/asset/search")
    @NoNeedLogin
    public ResponseDTO<ESSearchResultDTO<AssetResultVO>> login(@Valid @RequestBody AssetSearchDTO assetSearchDTO) {
        return searchService.searchByPage(assetSearchDTO);
    }
}
