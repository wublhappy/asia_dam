package com.gap.dam.module.business.search;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.lang.UUID;
import cn.hutool.core.util.RandomUtil;
import com.alibaba.fastjson.JSON;
import com.gap.DamFrontEndApplication;
import com.gap.dam.common.base.ResponseDTO;
import com.gap.dam.constant.ElasticSearchConst;
import com.gap.dam.module.business.asset.AssetService;
import com.gap.dam.module.business.asset.domain.dto.AssetAddDTO;
import com.gap.dam.module.business.asset.domain.vo.AssetVO;
import com.gap.dam.module.business.category.CategoryService;
import com.gap.dam.module.business.category.domain.dto.CategoryDTO;
import com.gap.dam.module.business.category.domain.vo.CategoryVO;
import com.gap.dam.module.business.search.domain.dto.Asset;
import com.gap.dam.module.business.search.domain.dto.AssetSearchDTO;
import com.gap.dam.module.business.search.domain.vo.AssetResultVO;
import com.gap.dam.module.support.file.constant.FileServiceNameConst;
import com.gap.dam.module.support.file.service.IFileService;
import com.gap.dam.util.BeanUtil;
import com.gap.dam.util.ESSearchResultDTO;
import com.github.javafaker.Faker;
import org.apache.commons.compress.utils.Lists;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Stack;
import java.util.stream.Collectors;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = DamFrontEndApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class RestHighLevelClientApplicationTests {

    @Autowired
    private AssetSearchService assetSearchService;

    @Autowired
    private AssetService assetService;

    @Autowired
    @Qualifier(FileServiceNameConst.ALI_OSS)
    private IFileService fileService;

    @Autowired
    private CategoryService categoryService;

//    @Before
//    public void startup() {
//        assetSearchService = new AssetSearchService();
//    }

    @Test
    public void contextLoads() {
        System.out.println(111);
    }

    @Test
    public void createIndex() {
        assetSearchService.createIndex(ElasticSearchConst.INDEX_NAME);
    }

    @Test
    public void deleteIndex() {
        assetSearchService.deleteIndex(ElasticSearchConst.INDEX_NAME);
    }

    @Test
    public void testInsert() {
        Faker faker = new Faker(Locale.CHINA);

        String[] assetTypeTags = new String[] {"Photography", "Others"};
        String[] fileTypeTags = new String[] {"JPEG"};
        String[] ccNumberTags = new String[] {"656341008", "656341009", "656341007", "656341006"};
        String[] modelTags = new String[] {"Single", "Couple", "Family", "Group"};
        String[] expirationTags = new String[] {"Expired", "Available"};
        String[] yearTags = new String[] {"2021"};
        String[] seasonTags = new String[] {"Fall", "Summer", "Spring"};
        String[] collectionTags = new String[] {"Heavy Gauge", "Kila Gauge"};
        String[] ipTags = new String[] {"Disney", "Mickey Mouse"};
        String[] kolTags = new String[] {"Lisa", "Ada", "Lucy"};

        String[] subBrand = new String[] {"Women", "Men", "Baby", "Student"};
        String[] styleNumber = new String[] {"656341", "656342", "656343"};
        String[] modelFigure = new String[] {"165cm/50kg", "170cm/60kg", "180cm/70kg"};
        String[] modelBwh = new String[] {"92/60/88", "92/66/88", "92/66/90"};
        String[] fileSize = new String[] {"20MB", "30MB", "40MB", "55MB", "70.69MB", "41.11MB"};


        String[][] category = new String[][] {
                {"China EC/2021/Summer/微淘/平面", "33"},
                {"China EC/2021/Summer/微淘/", "9"},
        };
        String[][] videoCategory = new String[][] {
                {"China EC/2021/Summer/微淘/视频", "34"}
        };
        String[][] videoFilepath = new String[][] {
                {"34", "asset/007EA7CE16192E45CDC7B32D7136EEBA.mp4"}
        };

        String[][] filePath = new String[][] {
                {"26", "asset/0DC8A61ABEE7E5EF45CC9E86F7C4F3CC.jpg"},
                {"27", "asset/12DC5720226ED77D0C4ACE8C7A22EB22.jpg"},
                {"28", "asset/18BCB66B85FA045AFF379E0B60775FE9.jpg"},
                {"29", "asset/5581E5C63A42271EA8EA566F4C915E22.jpg"},
                {"30", "asset/5F0C123A673B8DB14BEC091D9C3B9075.jpg"},
                {"31", "asset/CEAE6488DED9B7C18B1F5D4AF5971302.jpg"},
                {"32", "asset/D0C967D0A1B99B4074FC44B47AD89066.jpg"},
                {"33", "asset/D7B26CEFFCD4DE05D9118ACA70937D5F.jpg"}
        };

        List<Asset> assetList = new ArrayList<>();
        List<CategoryDTO> categoryVOList = categoryService.queryAllWithoutTree2().getData();
        for (CategoryDTO categoryVO : categoryVOList) {
            Asset asset = new Asset();

            asset.setId(UUID.fastUUID().toString());
            asset.setFileType("category");
            asset.setCategory(categoryVO.getCategoryName());
            asset.setCategoryId(Integer.valueOf(categoryVO.getId().toString()));
            asset.setParentCategoryId(Integer.valueOf(categoryVO.getParentId().toString()));
            asset.setParentCategoryIdList(getParentIdList(categoryVO, categoryVOList));
            ResponseDTO<String> urlResponseDTO = fileService.getFileUrl("asset/D7B26CEFFCD4DE05D9118ACA70937D5F.jpg");
            asset.setCategoryPictureUrl(urlResponseDTO.getData());

//            System.out.println(asset.getCategory() + ":" + asset.getParentCategorys());
            if (categoryVO.getParentId() == 0) {
                asset.setCategoryPictureUrl(fileService.getFileUrl(categoryVO.getFilePath()).getData());
                categoryService.updateCategoryPictureUrl(categoryVO.getId(), asset.getCategoryPictureUrl());
            }

            assetList.add(asset);
        }

        for (int i = 0; i < 200; i++) {
            Asset asset = new Asset();
            asset.setName(faker.name().title());
            asset.setCreateTime(faker.date().birthday());
            asset.setUpdateTime(faker.date().birthday());

            int categoryIndex = RandomUtil.randomInt(0, 1);
            asset.setCategory(category[categoryIndex][0]);
            asset.setCategoryId(Integer.valueOf(category[categoryIndex][1]));
            asset.setParentCategoryId(Integer.valueOf(category[categoryIndex][1]));

            CategoryDTO categoryDTO = new CategoryDTO();
            categoryDTO.setParentId(Long.valueOf(category[categoryIndex][1]));
            asset.setParentCategoryIdList(getParentIdList(categoryDTO, categoryVOList));
            // 打标签
            String assetType = assetTypeTags[RandomUtil.randomInt(0, 1)];
            String fileType = fileTypeTags[0];
            String ccNumber = ccNumberTags[RandomUtil.randomInt(0, 3)];
            String modelTag = modelTags[RandomUtil.randomInt(0, 3)];
            String expirationTag = expirationTags[RandomUtil.randomInt(0, 1)];
            String yearTag = yearTags[0];
            String season = seasonTags[RandomUtil.randomInt(0, 2)];
            String collection = collectionTags[RandomUtil.randomInt(0, 1)];
            String ip = ipTags[RandomUtil.randomInt(0, 1)];
            String kol = kolTags[RandomUtil.randomInt(0, 2)];

            asset.appendTag(assetType);
            asset.appendTag(fileType);
            asset.appendTag(ccNumber);
            asset.appendTag(modelTag);
            asset.appendTag(expirationTag);
            asset.appendTag(yearTag);
            asset.appendTag(season);
            asset.appendTag(collection);
            asset.appendTag(ip);
            asset.appendTag(kol);

            // 生成文件URL
            int randomFileIndex = RandomUtil.randomInt(0, 7);
            ResponseDTO<String> urlResponseDTO = fileService.getFileUrl(filePath[randomFileIndex][1]);
            asset.setUrl(urlResponseDTO.getData());
            if (filePath[randomFileIndex][1].endsWith("jpg"))
                asset.setFileType("jpeg");
            if (filePath[randomFileIndex][1].endsWith("mp4"))
                asset.setFileType("mp4");

            // Asset保存到数据库
            AssetAddDTO assetAddDTO = BeanUtil.copy(asset, AssetAddDTO.class);
            assetAddDTO.setFileId(Long.valueOf(filePath[randomFileIndex][0]));
            assetAddDTO.setIp(ip);
            assetAddDTO.setSubBrand(subBrand[RandomUtil.randomInt(0, 2)]);
            assetAddDTO.setSeason(season);
            assetAddDTO.setStyleNumber(styleNumber[RandomUtil.randomInt(0, 2)]);
            assetAddDTO.setYear(yearTag);
            assetAddDTO.setCcNumber(ccNumber);
            assetAddDTO.setCollection(collection);
            assetAddDTO.setKol(kol);
            assetAddDTO.setModel(modelTag);
            assetAddDTO.setModelFigure(modelFigure[RandomUtil.randomInt(0, 2)]);
            assetAddDTO.setModelBwh(modelBwh[RandomUtil.randomInt(0, 2)]);
            assetAddDTO.setAssetType(assetType);
            assetAddDTO.setGapDepartment("Marketing");
            assetAddDTO.setUsageRestrictions("Digital / Social / New Media");
            assetAddDTO.setRegionalUsage("Asia Use");
            assetAddDTO.setUsageStartDate(DateUtil.parse("2021-6-29", "yyyy-MM-dd"));
            assetAddDTO.setUsageExpiration(DateUtil.parse("2022-6-29", "yyyy-MM-dd"));
            assetAddDTO.setStatus("Published");
            assetAddDTO.setFileSize(fileSize[RandomUtil.randomInt(0, 5)]);
            assetAddDTO.setDimensions("5654*7469 px");
            assetAddDTO.setColorSpace("RGB");
            assetAddDTO.setFileType(fileType);
            assetAddDTO.setFileFormat("RGB 300");
            assetAddDTO.setCampaignName("Meet me in the Gap");
            assetAddDTO.setBelonging("Social");
            assetAddDTO.setRetouchingProjectNumber(384652);
            assetAddDTO.setDateCreated(DateUtil.parse("2021-6-29", "yyyy-MM-dd"));
            assetAddDTO.setUploaded(DateUtil.parse("2021-6-29 11:52:00", "yyyy-MM-dd HH:mm:ss"));

            ResponseDTO<AssetVO> assetVOResponseDTO = assetService.addAsset(assetAddDTO);
            asset.setId(String.valueOf(assetVOResponseDTO.getData().getId()));

            assetList.add(asset);
        }

        // 添加视频
//        Asset asset = new Asset();
//        asset.setName(faker.name().title());
//        asset.setCreateTime(faker.date().birthday());
//        asset.setUpdateTime(faker.date().birthday());
//
//        asset.setCategory(videoCategory[0][0]);
//        asset.setCategoryId(Integer.valueOf(videoCategory[0][1]));
//        asset.setParentCategoryId(Integer.valueOf(videoCategory[0][1]));
//
//        CategoryDTO categoryDTO = new CategoryDTO();
//        categoryDTO.setParentId(Long.valueOf(videoCategory[0][1]));
//        asset.setParentCategoryIdList(getParentIdList(categoryDTO, categoryVOList));
//        asset.appendTag(tags0[RandomUtil.randomInt(0, 2)]);
//        asset.appendTag(tags1[RandomUtil.randomInt(0, 3)]);
//        asset.appendTag(tags2[RandomUtil.randomInt(0, 1)]);

//        // 生成文件URL
//        ResponseDTO<String> urlResponseDTO = fileService.getFileUrl(videoFilepath[0][1]);
//        asset.setUrl(urlResponseDTO.getData());
//        asset.setFileType("mp4");

        // Asset保存到数据库
//        AssetAddDTO assetAddDTO = BeanUtil.copy(asset, AssetAddDTO.class);
//        assetAddDTO.setFileId(Long.valueOf(videoFilepath[0][0]));
//        ResponseDTO<AssetVO> assetVOResponseDTO = assetService.addAsset(assetAddDTO);
//        asset.setId(String.valueOf(assetVOResponseDTO.getData().getId()));

//        assetList.add(asset);

//         索引Asset
        assetSearchService.insert(ElasticSearchConst.INDEX_NAME, assetList);
    }

    private static List<Long> getParentIdList(CategoryDTO categoryDTO, List<CategoryDTO> categoryDTOList) {
        List<Long> parentCategoryIdList = Lists.newArrayList();
        while (categoryDTO != null
                && categoryDTO.getParentId() != 0
                && categoryDTO.getParentId() != null) {
            parentCategoryIdList.add(categoryDTO.getParentId());
            categoryDTO = findCategoryById(categoryDTO.getParentId(), categoryDTOList);
        }

        return parentCategoryIdList;
    }

    private static CategoryDTO findCategoryById(Long id, List<CategoryDTO> categoryDTOList) {
        for (CategoryDTO categoryDTO : categoryDTOList) {
            if (categoryDTO.getId().equals(id)) {
                return categoryDTO;
            }
        }

        return null;
    }


    @Test
    public void testUpdate() {
        List<Asset> assets = new ArrayList<>();
        Asset asset = new Asset();
        asset.setId(String.valueOf(1));
        asset.setName("Wubingli");
        asset.appendTag("China");
        asset.appendTag("GAP");
        asset.appendTag("Nanjing");
        asset.appendTag("Man");
        assets.add(asset);

        assetSearchService.update(ElasticSearchConst.INDEX_NAME, assets);
    }

    @Test
    public void searchList() {
        List<Asset> assets = assetSearchService.searchAll(ElasticSearchConst.INDEX_NAME);
        assets.forEach(asset -> {
            System.out.println(asset.toString());
        });
    }

    @Test
    public void searchByPage() {
        AssetSearchDTO searchDTO = new AssetSearchDTO();
//        searchDTO.setName("Dynamic");
        searchDTO.setPageNum(0);
        searchDTO.setPageSize(10);
        List<String> tags = new ArrayList<>();
//        tags.add("Single");
//        tags.add("Photography");
//        searchDTO.setTags(tags);
        searchDTO.setCategory("9");

        ResponseDTO<ESSearchResultDTO<AssetResultVO>> page = assetSearchService.searchAssetByPage(searchDTO);
        System.out.println(page.getData().getTotal());
        System.out.println(JSON.toJSONString(page, true));
    }

    @Test
    public void delete() {
        Asset asset = new Asset();
        asset.setId(String.valueOf(3));
        assetSearchService.delete(ElasticSearchConst.INDEX_NAME, asset);
    }

    @Test
    public void deleteAll() {
        assetSearchService.deleteAll(ElasticSearchConst.INDEX_NAME);
    }

    @Test
    public void testGroupSearch() {
        assetSearchService.groupByTags();
    }
}
