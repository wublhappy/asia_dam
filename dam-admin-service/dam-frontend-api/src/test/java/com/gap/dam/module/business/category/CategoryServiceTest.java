package com.gap.dam.module.business.category;

import com.alibaba.fastjson.JSON;
import com.gap.DamFrontEndApplication;
import com.gap.dam.common.base.ResponseDTO;
import com.gap.dam.module.business.category.domain.vo.CategoryVO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = DamFrontEndApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class CategoryServiceTest {
    @Autowired
    private CategoryService categoryService;

    @Test
    public void testQueryAll() {
        ResponseDTO<List<CategoryVO>> result = categoryService.queryAll();
        System.out.println(JSON.toJSONString(result, true));
    }
}
