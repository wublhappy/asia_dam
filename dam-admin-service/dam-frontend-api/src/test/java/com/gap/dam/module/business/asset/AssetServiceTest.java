package com.gap.dam.module.business.asset;

import cn.hutool.json.JSONUtil;
import com.alibaba.fastjson.JSON;
import com.gap.DamFrontEndApplication;
import com.gap.dam.common.base.ResponseDTO;
import com.gap.dam.module.business.asset.domain.dto.AssetAddDTO;
import com.gap.dam.module.business.asset.domain.vo.AssetVO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = DamFrontEndApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class AssetServiceTest {

    @Autowired
    private AssetService assetService;


    @Test
    public void testAddAsset() {
        AssetAddDTO assetAddDTO = new AssetAddDTO();
        assetAddDTO.setName("111");
        assetAddDTO.setAssetType("Video");
        assetAddDTO.setColorSpace("RGB");
        ResponseDTO<AssetVO> responseDTO = assetService.addAsset(assetAddDTO);
        System.out.println(JSON.toJSONString(responseDTO, true));
    }

    @Test
    public void testQueryAsset() {
        ResponseDTO<AssetVO> responseDTO = assetService.queryById(3670L);
//        System.out.println(JSONUtil.toJsonPrettyStr(responseDTO.getData()));
        System.out.println(JSONUtil.toJsonPrettyStr(responseDTO));
    }

    @Test
    public void testDownload() {
    }
}
