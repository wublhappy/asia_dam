package com.gap.dam.module.business.asset;

import com.gap.DamFrontEndApplication;
import com.gap.dam.common.base.ResponseDTO;
import com.gap.dam.module.support.file.constant.FileServiceNameConst;
import com.gap.dam.module.support.file.domain.vo.UploadVO;
import com.gap.dam.module.support.file.service.IFileService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.FileInputStream;
import java.io.IOException;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = DamFrontEndApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class AssetFileServiceTest {

    @Autowired
    @Qualifier(FileServiceNameConst.ALI_OSS)
    private IFileService fileService;

    @Test
    public void testFileUpload() throws IOException {
        String fileName = "E:\\assets\\IMG_20171223_160657.jpg";
        MockMultipartFile file = new MockMultipartFile(
                "file", fileName, "image/jpeg", new FileInputStream(fileName)
        );
        ResponseDTO<UploadVO> responseDTO = fileService.fileUpload(file, "asset");
        System.out.println(responseDTO);
    }

    @Test
    public void testFileDownload() {
//        fileService.downloadMethod()
    }
}
