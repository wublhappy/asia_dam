package com.gap.dam.module.business.search;

import cn.hutool.core.date.DateUtil;
import lombok.AllArgsConstructor;
import lombok.Data;

public class Test {
    public static void main(String[] args) {
        System.out.println(DateUtil.formatDateTime(DateUtil.date().toJdkDate()));
        System.out.println(DateUtil.current());
    }

    @Data
    @AllArgsConstructor
    static class Student {


        private int age;

        private String name;
    }
}
