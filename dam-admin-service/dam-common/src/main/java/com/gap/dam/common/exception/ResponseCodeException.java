package com.gap.dam.common.exception;

/**
 * [ 全局异常拦截后保留ResponseCode码的异常]
 *
 */
public class ResponseCodeException extends RuntimeException{
    private Integer code;

    public ResponseCodeException(Integer code, String message) {
        super(message);
        this.code = code;
    }

    public Integer getCode() {
        return code;
    }
}
