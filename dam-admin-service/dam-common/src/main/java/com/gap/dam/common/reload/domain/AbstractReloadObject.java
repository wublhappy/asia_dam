package com.gap.dam.common.reload.domain;

import com.gap.dam.common.reload.domain.entity.ReloadItem;
import com.gap.dam.common.reload.domain.entity.ReloadResult;

import java.io.PrintWriter;
import java.io.StringWriter;

/**
 * AbstractReloadObject 处理程序的抽象类
 *
 */
public abstract class AbstractReloadObject {

    protected String getStackTrace(Throwable e) {
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        e.printStackTrace(pw);
        return sw.toString();
    }

    /**
     * 通过reloadItem参数reload，获得结果
     *
     * @param reloadItem
     * @return boolean
     * @author zhuokongming
     * @date 2016年10月21日 下午2:09:44
     */
    public abstract ReloadResult reload(ReloadItem reloadItem);
}
