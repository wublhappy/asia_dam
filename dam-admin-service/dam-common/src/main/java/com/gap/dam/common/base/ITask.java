package com.gap.dam.common.base;

/**
 * [  ]
 *
 */
public interface ITask {

    void execute(String paramJson) throws Exception;
}
