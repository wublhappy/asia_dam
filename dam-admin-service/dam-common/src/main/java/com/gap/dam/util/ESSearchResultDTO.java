package com.gap.dam.util;

import com.gap.dam.common.base.PageResultDTO;
import lombok.Data;

import java.util.List;

@Data
public class ESSearchResultDTO<T> extends PageResultDTO<T> {

    /**
     * 标签查询数据
     */
    private List<TagTypeDTO> tagTypeList;
}
