package com.gap.dam.util;

import org.apache.commons.codec.digest.DigestUtils;

public class DigestUtil extends DigestUtils {

    /**
     * md5加盐加密
     *
     * @param salt
     * @param password
     * @return
     */
    public static String encryptPassword(String salt, String password) {
        return DigestUtil.md5Hex(String.format(salt, password));
    }
}
