package com.gap.dam.common.login;

import com.gap.dam.common.base.BaseBO;
import lombok.Getter;


@Getter
public class RequestTokenBO<T extends BaseBO> {

    private Long requestUserId;

    private T bo;

    public RequestTokenBO(T employeeBO) {
        this.requestUserId = employeeBO.getId();
        this.bo = employeeBO;
    }

    @Override
    public String toString() {
        return "RequestTokenBO{" +
                "requestUserId=" + requestUserId +
                ", bo=" + bo +
                '}';
    }
}
