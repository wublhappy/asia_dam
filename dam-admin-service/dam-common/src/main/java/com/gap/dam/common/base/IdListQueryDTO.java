package com.gap.dam.common.base;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
public class IdListQueryDTO {

    @ApiModelProperty(value = "id array")
    private List<Long> idArray;
}
