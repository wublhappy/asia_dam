package com.gap.dam.common.reload.domain;

import com.gap.dam.common.reload.domain.entity.ReloadItem;
import com.gap.dam.common.reload.domain.entity.ReloadResult;
import com.gap.dam.common.reload.interfaces.Reloadable;

/**
 * Reload 处理程序的实现类
 * 用于处理以接口实现的处理类
 *
 */
public class InterfaceReloadObject extends AbstractReloadObject {

    private Reloadable object;

    public InterfaceReloadObject(Reloadable object) {
        super();
        this.object = object;
    }

    @Override
    public ReloadResult reload(ReloadItem reloadItem) {
        ReloadResult reloadResult = new ReloadResult();
        reloadResult.setArgs(reloadItem.getArgs());
        reloadResult.setIdentification(reloadItem.getIdentification());
        reloadResult.setTag(reloadItem.getTag());
        try {
            boolean res = object.reload(reloadItem);
            reloadResult.setResult(res);
        } catch (Throwable e) {
            reloadResult.setException(getStackTrace(e));
        }
        return reloadResult;
    }

}
