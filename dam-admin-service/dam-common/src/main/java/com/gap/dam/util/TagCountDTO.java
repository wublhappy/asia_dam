package com.gap.dam.util;

import lombok.Data;

@Data
public class TagCountDTO {

    private String tagName;

    private int docCount;
}
