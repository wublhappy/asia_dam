package com.gap.dam.common.base;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

/**
 * @program: gap-webdam
 * @description:
 * @author: Bing Li Wu
 * @create: 2021/7/5 23:51
 **/
@Getter
@Setter
public class BaseBO {
    /**
     * 主键ID
     */
    private Long id;
}
