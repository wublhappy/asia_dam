package com.gap.dam.util;

import lombok.Data;

import java.util.List;

@Data
public class TagTypeDTO {
    /**
     * Tag分类
     */
    private String tagType;

    /**
     * Tag列表
     */
    private List<TagCountDTO> tagList;
}
