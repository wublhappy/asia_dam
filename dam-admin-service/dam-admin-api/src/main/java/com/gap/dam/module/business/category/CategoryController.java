package com.gap.dam.module.business.category;

import com.gap.dam.common.anno.NoValidPrivilege;
import com.gap.dam.common.anno.OperateLog;
import com.gap.dam.common.base.ResponseDTO;
import com.gap.dam.constant.SwaggerTagConst;
import com.gap.dam.module.business.category.domain.dto.CategoryAddDTO;
import com.gap.dam.module.business.category.domain.dto.CategoryUpdateDTO;
import com.gap.dam.module.business.category.domain.vo.CategoryVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
@OperateLog
@Api(tags = {SwaggerTagConst.Admin.MANAGER_CATEGORY})
public class CategoryController {

    @Autowired
    private CategoryService categoryService;


    @ApiOperation(value = "查询所有Category",notes = "")
    @PostMapping("/category/query")
    @NoValidPrivilege
    public ResponseDTO<List<CategoryVO>> queryAll() {
        return categoryService.queryAll();
    }

    @ApiOperation(value = "添加Category",notes = "")
    @PostMapping("/category/add")
    @NoValidPrivilege
    public ResponseDTO<Long> add(@RequestBody @Valid CategoryAddDTO addDTO){
        return categoryService.addCategory(addDTO);
    }

    @ApiOperation(value = "编辑Category",notes = "")
    @PostMapping("/category/update")
    @NoValidPrivilege
    public ResponseDTO<String> update(@RequestBody @Valid CategoryUpdateDTO updateDTO){
        return categoryService.updateCategory(updateDTO);
    }

    @ApiOperation(value = "删除Category",notes = "")
    @PostMapping("/category/delete/{id}")
    @NoValidPrivilege
    public ResponseDTO<String> delete(Long id){
        return categoryService.deleteById(id);
    }

}
