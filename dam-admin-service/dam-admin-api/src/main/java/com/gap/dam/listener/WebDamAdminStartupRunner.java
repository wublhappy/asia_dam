package com.gap.dam.listener;

import com.gap.dam.constant.ResponseCodeConst;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

/**
 * 应用启动以后检测code码
 */

@Component
public class WebDamAdminStartupRunner implements CommandLineRunner {

    @Override
    public void run(String... args) {
        ResponseCodeConst.init();
    }
}