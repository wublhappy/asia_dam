package com.gap.dam.module.business.asset;

import com.gap.dam.common.anno.OperateLog;
import com.gap.dam.constant.SwaggerTagConst;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

@RestController
@OperateLog
@Api(tags = {SwaggerTagConst.Admin.MANAGER_CATEGORY})
public class AssetController {

    @Autowired
    private AssetService assetService;


}
