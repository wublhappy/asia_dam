package com.gap.dam.module.business.filemanage;

import com.gap.dam.common.anno.OperateLog;
import com.gap.dam.common.base.PageResultDTO;
import com.gap.dam.common.base.ResponseDTO;
import com.gap.dam.constant.SwaggerTagConst;
import com.gap.dam.module.business.filemanage.domain.constant.FileManageResponseCodeConst;
import com.gap.dam.module.business.filemanage.domain.dto.*;
import com.gap.dam.module.business.filemanage.domain.vo.FileVO;
import com.gap.dam.module.business.tag.domain.dto.TagQueryDTO;
import com.gap.dam.module.support.file.constant.FileModuleTypeEnum;
import com.gap.dam.module.support.file.constant.FileServiceTypeEnum;
import com.gap.dam.module.support.file.domain.vo.UploadVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@RestController
@OperateLog
@Api(tags = {SwaggerTagConst.Admin.MANAGER_FILE_MANAGE})
public class FileManagerController {

    @Autowired
    private FileManageService fileManageService;


    @ApiOperation(value = "查询文件")
    @PostMapping("/fileManage/query")
    public ResponseDTO<PageResultDTO<FileVO>> queryFilePage(@RequestBody FileQueryDTO queryDTO) {
        return fileManageService.queryFileList(queryDTO);
    }

    @ApiOperation(value = "移动文件")
    @PostMapping("/fileManage/move")
    public ResponseDTO<String> moveFiles(@RequestBody FileMoveDTO fileMoveDTO) {
        return fileManageService.moveFiles(fileMoveDTO);
    }

    @ApiOperation(value = "重命名文件")
    @PostMapping("/fileManage/rename")
    public ResponseDTO<String> renameFile(@RequestBody FileRenameDTO fileRenameDTO) {
        return fileManageService.renameFile(fileRenameDTO);
    }

    @ApiOperation(value = "删除文件")
    @PostMapping("/fileManage/delete")
    public ResponseDTO<String> deleteFile(@RequestBody FileDelDTO fileDelDTO) {
        return fileManageService.deleteFile(fileDelDTO);
    }

    @ApiOperation(value = "新建文件夹")
    @PostMapping("/fileManage/createFolder")
    public ResponseDTO<Long> createNewFolder(@RequestBody FolderAddDTO folderAddDTO) {
        return fileManageService.createNewFolder(folderAddDTO);
    }

    @ApiOperation(value = "多文件本地上传", notes = "多文件本地上传")
    @PostMapping("/fileManage/localMultiUpload/{moduleType}/{parentFolderId}")
    public ResponseDTO<List<UploadVO>> localUpload(@RequestParam("files") MultipartFile[] file,
                                                   @PathVariable("moduleType") Integer moduleType,
                                                   @PathVariable("parentFolderId") Long parentFolderId) throws Exception {
        return fileManageService.uploadMultifiles(file, FileServiceTypeEnum.LOCAL, moduleType, parentFolderId);
    }

    @ApiOperation(value = "文件本地上传", notes = "文件本地上传")
    @PostMapping("/fileManage/localUpload/{moduleType}/{parentFolderId}")
    public ResponseDTO<UploadVO> localUpload(@RequestParam("files") MultipartFile file,
                                                   @PathVariable("moduleType") Integer moduleType,
                                                   @PathVariable("parentFolderId") Long parentFolderId) throws Exception {
        return fileManageService.uploadFile(file, FileServiceTypeEnum.LOCAL, moduleType, parentFolderId);
    }
}
