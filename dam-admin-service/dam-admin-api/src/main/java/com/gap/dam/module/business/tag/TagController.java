package com.gap.dam.module.business.tag;

import com.gap.dam.common.anno.NoValidPrivilege;
import com.gap.dam.common.anno.OperateLog;
import com.gap.dam.common.base.PageResultDTO;
import com.gap.dam.common.base.ResponseDTO;
import com.gap.dam.constant.SwaggerTagConst;
import com.gap.dam.module.business.category.domain.dto.CategoryAddDTO;
import com.gap.dam.module.business.category.domain.dto.CategoryUpdateDTO;
import com.gap.dam.module.business.category.domain.vo.CategoryVO;
import com.gap.dam.module.business.tag.domain.dto.TagAddDTO;
import com.gap.dam.module.business.tag.domain.dto.TagQueryDTO;
import com.gap.dam.module.business.tag.domain.dto.TagUpdateDTO;
import com.gap.dam.module.business.tag.domain.vo.TagVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
@OperateLog
@Api(tags = {SwaggerTagConst.Admin.MANAGER_TAG})
public class TagController {

    @Autowired
    private TagService tagService;

    @ApiOperation(value = "查询标签")
    @PostMapping("/tag/query")
    public ResponseDTO<PageResultDTO<TagVO>> queryAll(@RequestBody TagQueryDTO queryDTO) {
        return tagService.queryTagList(queryDTO);
    }

    @ApiOperation(value = "添加标签")
    @PostMapping("/tag/add")
    public ResponseDTO<Long> add(@RequestBody @Valid TagAddDTO addDTO){
        return tagService.addTag(addDTO);
    }

    @ApiOperation(value = "更新标签")
    @PostMapping("/tag/update")
    public ResponseDTO<String> update(@RequestBody @Valid TagUpdateDTO updateDTO){
        return tagService.updateTagById(updateDTO);
    }

    @ApiOperation(value = "删除标签")
    @PostMapping("/tag/delete/{id}")
    public ResponseDTO<String> delete(Long id){
        return tagService.deleteById(id);
    }
}
