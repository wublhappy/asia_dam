package com.gap.dam.module.business.tag;

import com.gap.dam.common.anno.NoValidPrivilege;
import com.gap.dam.common.anno.OperateLog;
import com.gap.dam.common.base.ResponseDTO;
import com.gap.dam.constant.SwaggerTagConst;
import com.gap.dam.module.business.tag.domain.vo.TagTypeVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@OperateLog
@Api(tags = {SwaggerTagConst.Admin.MANAGER_TAG_TYPE})
public class TagTypeController {
    @Autowired
    private TagTypeService tagTypeService;

    @ApiOperation(value = "查询所有标签类型")
    @PostMapping("/tagtype/query")
    @NoValidPrivilege
    public ResponseDTO<List<TagTypeVO>> queryAll() {
        return tagTypeService.queryAll();
    }
}
