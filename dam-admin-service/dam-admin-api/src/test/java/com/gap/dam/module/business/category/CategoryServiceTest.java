package com.gap.dam.module.business.category;

import com.alibaba.fastjson.JSON;
import com.gap.dam.DamAdminApplication;
import com.gap.dam.common.base.ResponseDTO;
import com.gap.dam.module.business.category.domain.dto.CategoryAddDTO;
import com.gap.dam.module.business.category.domain.vo.CategoryVO;
import com.gap.dam.module.system.employee.domain.dto.EmployeeLoginFormDTO;
import com.gap.dam.module.system.login.LoginService;
import com.gap.dam.module.system.login.LoginTokenService;
import com.gap.dam.module.system.login.domain.LoginDetailVO;
import io.jsonwebtoken.lang.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = DamAdminApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class CategoryServiceTest {
    @Autowired
    private CategoryService categoryService;

    @Autowired
    private LoginService loginService;

    private MockHttpServletRequest request;

    private MockHttpServletResponse response;

    @Before
    public void setUp() {
        request = new MockHttpServletRequest();
        request.setCharacterEncoding("UTF-8");
        EmployeeLoginFormDTO employeeLoginFormDTO = new EmployeeLoginFormDTO();
        employeeLoginFormDTO.setLoginName("sa");
        employeeLoginFormDTO.setLoginPwd("123456");
        ResponseDTO<LoginDetailVO> responseDTO = loginService.login(employeeLoginFormDTO, request);
        System.out.println(responseDTO.getData().getXAccessToken());
    }

    @Test
    public void testQueryAll() {
        ResponseDTO<List<CategoryVO>> result = categoryService.queryAll();
        System.out.println(JSON.toJSONString(result, true));
    }

    @Test
    public void testAddCategory() {
        CategoryAddDTO categoryAddDTO = new CategoryAddDTO();
        categoryAddDTO.setParentId(37L);
        categoryAddDTO.setCategoryName("南京");
        categoryAddDTO.setSort(2);
        ResponseDTO<Long> responseDTO = categoryService.addCategory(categoryAddDTO);
        System.out.println(responseDTO.getData());
    }
}
