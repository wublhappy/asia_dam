package com.gap.dam;

import com.gap.dam.module.support.file.constant.FileModuleTypeEnum;
import com.gap.dam.util.BaseEnumUtil;
import org.junit.After;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * 测试基类
 *
 * @author lizongliang
 * @date 2017/09/29 10:54
 */
//@RunWith(SpringRunner.class)
//@SpringBootTest(classes = DamAdminApplication.class,webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class BaseTest {

    public static void main(String[] args) {
        FileModuleTypeEnum moduleTypeEnum = BaseEnumUtil.getEnumByValue(3, FileModuleTypeEnum.class);
        System.out.println(moduleTypeEnum.getDesc());
    }
}
