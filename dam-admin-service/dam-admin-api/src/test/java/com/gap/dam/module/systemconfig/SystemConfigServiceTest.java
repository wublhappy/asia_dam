package com.gap.dam.module.systemconfig;

import com.gap.dam.DamAdminApplication;
import com.gap.dam.common.base.ResponseDTO;
import com.gap.dam.constant.SystemConfigGroupConstant;
import com.gap.dam.module.system.systemconfig.SystemConfigService;
import com.gap.dam.module.system.systemconfig.domain.dto.SystemConfigDTO;
import com.gap.dam.module.system.systemconfig.domain.dto.SystemConfigVO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = DamAdminApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class SystemConfigServiceTest {

    @Autowired
    private SystemConfigService systemConfigService;

    @Test
    public void testQuery() {
        ResponseDTO<List<SystemConfigVO>> responseDTO = systemConfigService.getListByGroup(SystemConfigGroupConstant.FILE_TYPE_KEY);
        responseDTO.getData().forEach(systemConfigVO -> {
            System.out.println(systemConfigVO.getConfigValue());
        });
    }
}
