package com.gap.dam.module.filemanage;

import com.gap.dam.DamAdminApplication;
import com.gap.dam.module.business.filemanage.FileManageService;
import com.gap.dam.module.business.filemanage.domain.entity.FolderEntity;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = DamAdminApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class FileManageServiceTest {
    @Autowired
    private FileManageService fileManageService;

    @Test
    public void testFolderChildren() {
        List<FolderEntity> folderEntityList = fileManageService.queryFolderList(null);
        FolderEntity rootEntity = new FolderEntity();
        rootEntity.setId(2L);
        rootEntity.setFolderName("a");
        rootEntity.setParentFolderId(0L);
        List<FolderEntity> children = fileManageService.findChildren(rootEntity, folderEntityList);
        Assert.assertTrue(children.size() > 0);
        children.stream().map(FolderEntity::getFolderName).forEach(System.out::println);
    }
}
